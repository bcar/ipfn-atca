/*  Readme file for ATCA-Dacq test programs 
 last changes, May 23, 2016
*/

**** atcadaq2 test program
To compile and link the test program "atcadaq2" only type "make all2" on the command line
The program "atcadaq2" has at least two mandatory command line parameter -524 or -1048 or -2096 and "-sw" or "-hw"
e.g. "atcadaq -524 -sw" or "atcadaq -1048 -hw"
The first parameter is the desired acquisiton time in milliseconds:
Three values are possible: 524, 1048, 2096, signifing 524 ms 1048 ms and 2096 ms
The default value is 1048 ms

"-sw" means a software triggered start of the data acquisition by pressing RETURN on the keyboard
"-hw" means a hardware triggered start of the data acquisition with a 5 Volt TTL high-low transition signal

e.g. "atcadaq2 -524 -sw" makes an acquistion for 524 ms with a software trigger (keyboard RETURN)
e.g. "atcadaq2 -1048 -hw" makes an acquistion for 1048 ms with a hardware trigger (waiting for hardware signal transition)

A third parameter can be supplied also for the device name of the used board "/dev/atcaAdcStream.xx"
e.g. "atcadaq2 -524 -sw /dev/atcaAdcStream.13"

The default device name is  "/dev/atcaAdcStream.14"

The test program stores for the desired acquisition time the acquired data for all 32 channels
as *.txt files name ch01.txt ... ch32.txt in a directory named with the date and time of the acquisition
e.g. "20151110153245" where the first four digits are the Year (2015), next 4 digits are Month (11) and day (10)
and the last 6 digits are Hours (15) Minutes (32) and Seconds (45)
So every acquisition made is located in its individual directory.

**** atcadaq test program
To compile and link the test program "atcadaq" only type "make" on the command line
The program "atcadaq" has at least one command line parameter "-sw" or "-hw"
e.g. "atcadaq -sw" or "atcadaq -hw"
"-sw" means a software triggered start of the data acquisition by pressing RETURN on the keyboard
"-hw" means a hardware triggered start of the data acquisition with a 5 Volt TTL high-low transition signal

A second parameter can be supplied also for the device name of the used board "/dev/atcaAdcStream.xx"
e.g. "atcadaq -sw /dev/atcaAdcStream.13"

The default device name is  "/dev/atcaAdcStream.14"


As default the test program stores the first complete DMA buffer of 2097152 samples (4194304 Bytes)
or 65536 samples for each of the 32 channels in a file named "data.txt".
To fill one DMA buffer at a sample rate of 2 MSPS takes 32,768 ms. So every second (1000 ms) about
30 DMA buffers are filled.

The version from October 2015 on, now supports both "Standard ADC Modules" and "Chopped ADC Modules"

The following is valid for atcadaq and atcadaq2 test programs:

The function which initializes the the data acquisition now has two additional paranmeters:

int
init_device(char *dev_name, int chopped, unsigned int chopperFreq)
{
 ..
}

1. int chopped has a value >0 when chopped modules are used, otherwise it is 0

2. unsigned int chopperFreq defines the chopper frequency (in Hz) when chopped modules are used
   The value of chopperFreq is only defined between 100 and 10000, otherwise a predefined standard value of
   2000 Hz is used.
   When chopperFreq has the value 0, chopped modules are operated like a "Standard module" 
   but with somewhat different properties (input stage differs)

The latest version from May 2016 on, allows to store the Chopper signal in channel 32 instead of ADC data
by using the following code snippet:

//Save Chopper signal in channel 32 (instead of ADC data)
 rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_CH32_ON);
 
 To switch off this feature use the following code snippet:
 
 //Save ADC data in channel 32, and NO Chopper signal)
 rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_CH32_OFF);
 
By default, the Chopper signal is NOT stored, but the ADC data in channel 32



