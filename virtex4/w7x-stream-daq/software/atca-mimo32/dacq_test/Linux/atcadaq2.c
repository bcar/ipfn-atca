#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/types.h>
#include "atcaAdc_ioctl.h"
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "atcalib.h"

//Global vars
int run=1;										// main loop flag
short acqData[4*N_AQS*N_PACKETS_2*NUM_CHAN];	// large buffer to store data for 2096 ms which can be saved to hard drive
unsigned int tmp; 								// generic
char errmsg[80];
int rc;											// generic
char *dev_name = "/dev/atcaAdcStream.14";
int acq_time;
int acq_time_524ms = 16;
int acq_time_1048ms  = 32;
int acq_time_2096ms  = 64;

void sigint_handler(int sig);
void getkey (void);

int chopped_modules;
unsigned int chopper_freq;

int main(int argc, char **argv) {

	/* SIGINT */
	struct sigaction sa;
	sa.sa_handler = sigint_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, NULL);
	/* /SIGINT */

	/* TIMER config (optional) */
	struct itimerval timer;
	timer.it_interval.tv_sec = TIMERVALUE;
	timer.it_interval.tv_usec = 0;
	timer.it_value.tv_sec = TIMERVALUE;
	timer.it_value.tv_usec = 0;
	setitimer(ITIMER_REAL, &timer, NULL);
	/* /TIMER config */

	unsigned int loss_hits; 		// counter for FPGA counter discontinuities (implies packet loss)
	unsigned int max_buffer_count;
	unsigned int fposition;			// position of read() in target array
	int i, loops;  					// loop iterators
	int fd; 						// file descriptor of device
	int hw_trigger = 0; 			// trigger scheme to start acquisition

 	char *arg = "-sw";
 
	memset(&acqData[0], 0, sizeof(acqData));  // Array to hold all acquired data
	printf("sizeof(acqData[]) = %lu bytes\n",sizeof(acqData));
	acq_time = acq_time_1048ms; 	// Default acquisition time is 1048 ms

	// Check command line
	if(argc < 3)
	{
        	printf("Usage: atcadaq -524 |-1048 |-2096 -hw|-sw  [dev_name]\n");
		exit(-1);
	}

	arg = argv[1];
	if (!strcmp(arg, "-524"))
		{
			acq_time = acq_time_524ms;
		}
	if (!strcmp(arg, "-1048"))
		{
			acq_time = acq_time_1048ms;
		}
	if (!strcmp(arg, "-2096"))
		{
			acq_time = acq_time_2096ms;
		}
	printf("arg1 = %s\n", arg);

    arg = argv[2];
    if (!strcmp(arg, "-hw"))
    {
        	hw_trigger = 1; 
	}
	printf("arg2 = %s\n", arg);



	if(argc > 3){
		dev_name = argv[3];
		printf("arg3 = %s\n", dev_name);
	}


	chopped_modules = 1;	// Chopped modules are used
	// chopper_freq = 0 -> Chopper modules are operated NOT chopped!
	// chopper_freq is in Hz; e.g. 1000 = 1KHz	
	//chopper_freq = 1000; 	
	chopper_freq = 2000; 
	//chopper_freq = 0; 
	//chopper_freq = 4000; 	

	printf("Exact acquisition time = %4.2f ms\n", acq_time*32768.0/1000.0);

	// Opening the device
	fd = init_device(dev_name, chopped_modules, chopper_freq);


	// Init
	i = 0;
	loops = 0;
	loss_hits = 0;

	// ADC board is armed and waiting for trigger (hardware or software)
	if (!hw_trigger)
	{
	  // rc = ioctl(fd, PCIE_ADC_IOCT_STREAM_ENABLE);
	  // rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);
	  // printf("FPGA Status: 0x%.8X\n", tmp);
		printf("software trigger mode active\n");
		getkey(); // Press any key to start the acquisition
		// Start the dacq with a software trigger
		rc = ioctl(fd, PCIE_ADC_IOCT_SOFT_TRIG); 
	    rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);
		printf("FPGA Status: 0x%.8X\n", tmp);
		// Check status to see if everything is okay (optional)
	}
	else
		printf("hardware trigger mode active\n");

	fflush(stdout);			
	fposition = 0;


	// acq_time = acq_time_524ms;
	//acq_time = acq_time_1048ms;

	while (loops++ < acq_time) {

		rc = 0;

		while (rc == 0) {

			rc = read(fd, &acqData[fposition], DMA_SIZE); // loop until there is something to read.
		}

		fposition += (DMA_SIZE/2);

		i++;
		
	} /* End while */

	/*
	this IOCTL returns the nr of times the driver IRQ handler was called
	while there was still 1 or more buffers waiting to be read
	Stop streaming and un-arm the FPGA.
	*/

	max_buffer_count = (unsigned int) ioctl(fd, PCIE_ADC_IOCT_ACQ_DISABLE);
	usleep(100);  // must wait to call next IOCTL... FPGA PCIendpoint apparently handles race conditions poorly
	loss_hits = (unsigned int) ioctl(fd, PCIE_ADC_IOCT_STREAM_DISABLE);

	// Check status to see if everything is okay (optional)
	rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);

	save_to_disk2 (acq_time, &acqData[0]);

	printf("Streaming off - FPGA Status: 0x%.8X %d mismatches %d max buffer count\n", tmp, loss_hits, max_buffer_count);

	/*
	printf("acqData[0]= %d\n", acqData[0]);
	printf("acqData[1]= %d\n", acqData[1]);
	printf("acqData[2]= %d\n", acqData[2]);
	printf("acqData[3]= %d\n", acqData[3]);
	printf("acqData[4]= %d\n", acqData[4]);
	printf("acqData[31]= %d\n", acqData[31]);

	printf("acqData[0+1048575*32]= %d\n", acqData[0+1048575*32]);
    printf("acqData[1+1048575*32]= %d\n", acqData[1+1048575*32]);
	printf("acqData[2+1048575*32]= %d\n", acqData[2+1048575*32]);
	printf("acqData[3+1048575*32]= %d\n", acqData[3+1048575*32]);
	printf("acqData[4+1048575*32]= %d\n", acqData[4+1048575*32]);
	printf("acqData[31+1048575*32]= %d\n", acqData[31+1048575*32]);
	printf("acqData[0+1048576*32]= %d\n", acqData[0+1048576*32]);
	*/

	tmp = close(fd);
	if (tmp==-1) {
		printf("Error closing : %s\n", strerror(errno));
	}
  return 0;
}

void sigint_handler(int sig)
{
  run=0;
  printf("INT received!\n");
  exit(0);
}

void
getkey ()
{
        printf("Press Return to continue ...\n");
        getchar();
}

