/*
 * Libarary for the ATCA-MIMO-ISOL ADC Board test software
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/types.h>
#include "atcaAdc_ioctl.h"
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "atcalib.h"


int
init_device(char *dev_name, int chopped, unsigned int chopperFreq)
{
  int fd, rc;
  unsigned int tmp;
  char * devn;
  int flags = 0;
  unsigned int chop_period;

  

  // Opening the device (new)
  devn = dev_name;
	
  flags |= O_RDONLY;
  printf("open device\n");
  extern int errno;
  fd=open(devn,flags);

  if (fd < 0)
    { fprintf (stderr,"Error: cannot open device %s \n", devn);
      fprintf (stderr," errno = %i\n",errno);
      printf ("open error : %s\n", strerror(errno));
      exit(1);
    }
  printf("device open: %i \n", fd); 
  // resets SW trigger
  ioctl(fd, PCIE_ADC_IOCT_ACQ_DISABLE);
  usleep(1000);
  rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_OFF);
  printf("Setting chopper off\n");

  if (chopped > 0)
  {
	  //Set the Chop off as default
	  rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_OFF);
	  printf("Setting chopper off\n");

	  rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_DEFAULT_0); //
	  printf("Setting chopper signal level to 0 (default)\n");

	  if ((chopperFreq >= 100) && (chopperFreq <= 10000))
	  {
		  // Examples of chopperFreq and chop_period:

		  // chopperFreq 10000 Hz -> chop_period = 200
		  // chopperFreq  5000 Hz -> chop_period = 400
		  // chopperFreq  4000 Hz -> chop_period = 500
		  // chopperFreq  2000 Hz -> chop_period = 1000
		  // chopperFreq  1000 Hz -> chop_period = 2000
		  // chopperFreq   500 Hz -> chop_period = 4000

		  // Calculate chop period;
		  chop_period = 2000000/chopperFreq; // Fchopper = 2Khz - chop_period = 1000
		  printf("Setting Fchopper = %d Hz - chop_period = %d clks\n", chopperFreq, chop_period);
	  }
	  else {
		  chop_period = 1000;
		  printf("Fchopper = %d Hz out of range (100Hz-10000Hz): Setting default Fchopper = %d Hz - chop_period = %d clks\n", chopperFreq, 2000, chop_period);
	  }
	  /* Set the Chop's period, in this case is 1000 times the period of the acquisition period.
	  	 The Chop's frequency will be 2kHz
	  */

	  tmp = chop_period;
	  rc  =  ioctl(fd, PCIE_ATCA_IOCS_CHOP_MAX_COUNT, &tmp);
	  tmp = chop_period/2;
	  rc = ioctl(fd, PCIE_ATCA_IOCS_CHOP_CHANGE_COUNT, &tmp);

	  /* If "chopperFreq" is greater 0 then the module is operated in "chopped" mode.
	  	 Otherwise it is behaving like a "Standard module" with somewhat different properties (input stage)
	  */
	  if (chopperFreq > 0)
	  {
	  		  //Set the Chop on
	  		  rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_ON);
	  		  printf("Setting chopper on: ADC modules are operated in chopped mode\n");
	  }


  }

  rc = ioctl(fd, PCIE_ADC_IOCT_ACQ_ENABLE);  	// Arm the FPGA to wait for external trigger
  rc = ioctl(fd, PCIE_ADC_IOCT_INT_ENABLE);  	// Enable interrupts
  rc = ioctl(fd, PCIE_ADC_IOCT_STREAM_ENABLE);  // enable streaming 

  rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);	// Get FPGA STATUS to check if properly initialized (optional)
  printf("FPGA Status: 0x%.8X\n", tmp);
  rc |= ioctl(fd, PCIE_ADC_IOCG_COUNTER, &tmp);	// Get FPGA sample counter - only increments after trigger so should be 0 here (optional)
  printf("FPGA Counter: 0x%.8X\n", tmp);
  printf("rc = %d\n",rc );
 
  return fd;
}


void  enable_save_to_disk( int *nrpk, int *nraq, int *acq)
{
	*nrpk=0;
	*nraq=0;
	*acq=1;
}

void save_to_disk(int *nrpk, int *nraq, int *acq, short *acqData, short *matData)
{
	FILE *stream_out;
	int numchan;
	// save buffer contents until we have gathered the specified number of full buffers to save to the hard drive
	if (*nrpk < N_PACKETS)
	{
		memcpy(&acqData[(*nrpk)*DMA_SIZE/2], &matData[0], DMA_SIZE);   // save the contents of the buffer into memory
		(*nrpk)++;
	}
	
	if ((*nrpk == N_PACKETS) && (*acq == 1))	// wait until we have gathered all the data we want to save
	{
		*nrpk = 0;
		*nraq = 0;
		stream_out = fopen("./data.txt","wt");
		for (*nrpk=0; *nrpk < N_PACKETS ; (*nrpk)++) {
			for (*nraq=0; *nraq < N_AQS ; (*nraq)++) {
				for (numchan=0 ; numchan < NUM_CHAN ; numchan++) {
					fprintf(stream_out, "%.4hd ",acqData[(*nrpk)*N_AQS*NUM_CHAN+ (*nraq)*NUM_CHAN+numchan]);
				}
				fprintf(stream_out, "\n");
			}
		}
		fflush(stream_out);
		fclose(stream_out);
		*acq = 0;
		}
}

// a: MSB, b: LSB
unsigned int concatenate(short a, short b)
{
 unsigned int retval;
 retval = ( (a << 16) & 0xFFFF0000) | b ;
 return retval;
}

