# Plot channels 25 - 32 of ATCA-MIMO-ISOL
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set xrange [0:8191]                
set xrange [0:1199]                
set x2range [0:599]                
#set xtics  0,200,1199
set yrange [-32768:32767] 
#set yrange [-14000:14000] 
#set yrange [-180:60] 
#set yrange [-115:90] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Test of IPP Board #14 - Fsignal = 5KHz-12Vpp - 06.08.2015 MMZ IPP/GAR"
#set title "ATCA-MIMO-ISOL Test of IPP Board #14 - Fsignal = GND-shortcut - 06.08.2015 MMZ IPP/GAR"
dataFile='data.txt'

plot dataFile using ($0):($25 +0 ) title 'channel 25' with lines, \
	dataFile using ($0):($26 * 1 +1000 ) title 'channel 26' with lines, \
	dataFile using ($0):($27 * 1 +2000 ) title 'channel 27' with lines, \
	dataFile using ($0):($28 * 1 +3000 ) title 'channel 28' with lines, \
	dataFile using ($0):($29 * 1 +4000 ) title 'channel 29' with lines, \
	dataFile using ($0):($30 * 1 +5000 ) title 'channel 30' with lines, \
	dataFile using ($0):($31 * 1 +6000 ) title 'channel 31' with lines, \
	dataFile using ($0):($32 * 1 +7000 ) title 'channel 32' with lines
	
 
