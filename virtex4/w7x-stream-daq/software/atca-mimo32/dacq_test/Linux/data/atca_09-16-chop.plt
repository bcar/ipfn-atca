# Plot channels 09 - 16 of ATCA-MIMO-ISOL
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
set term png enhanced font '/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf' 12
set output 'kian/atca_09-16-chop.png'

#set xrange [0:8191]                
set xrange [0:19999]                
set x2range [0:19999]                
#set xtics  0,200,1199
#set yrange [-32768:32767] 
set yrange [-35000:35000] 
#set yrange [-14000:14000] 
#set yrange [-115:90] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Test of IPP Board # - Fsignal = 100 Hz-3.4Vpp - 13.10.2015 MMZ IPP/HGW"
dataFile='data_09_16.txt'

plot dataFile using ($0):($9 * 1 +0 ) title 'channel 09' with lines, \
	dataFile using ($0):($10 * 1 +0 ) title 'channel 10' with lines, \
	dataFile using ($0):($11 * 1 +0 ) title 'channel 11' with lines, \
	dataFile using ($0):($12 * 1 +0 ) title 'channel 12' with lines, \
	dataFile using ($0):($13 * 1 +0 ) title 'channel 13' with lines, \
	dataFile using ($0):($14 * 1 +0 ) title 'channel 14' with lines, \
	dataFile using ($0):($15 * 1 +0 ) title 'channel 15' with lines, \
	dataFile using ($0):($16 * 1 +0 ) title 'channel 16' with lines
	
set term x11
#set term wxt
replot
#pause -1 "Hit return to continue"
