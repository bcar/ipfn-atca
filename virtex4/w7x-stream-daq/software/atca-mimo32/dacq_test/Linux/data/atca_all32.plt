# Plot all 32 channels of ATCA-MIMO-ISOL
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set xrange [0:8191]                
set xrange [0:1199]                
set x2range [0:599]                
#set xtics  0,200,1199
set yrange [-32768:32767] 
#set yrange [-14000:14000] 
#set yrange [-200:200] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Test of IPP Board #14 - Fsignal = 5KHz - 05.08.2015 MMZ IPP/GAR"
dataFile='data.txt'

plot dataFile using ($0):($1 * 1) title 'channel 01' with lines, \
	dataFile using ($0):($2 * 1 +0 ) title 'channel 02' with lines, \
	dataFile using ($0):($3 * 1 +0 ) title 'channel 03' with lines, \
	dataFile using ($0):($4 * 1 +0 ) title 'channel 04' with lines, \
	dataFile using ($0):($5 * 1 +0 ) title 'channel 05' with lines, \
	dataFile using ($0):($6 * 1 +0 ) title 'channel 06' with lines, \
	dataFile using ($0):($7 * 1 +0 ) title 'channel 07' with lines, \
	dataFile using ($0):($8 * 1 +0 ) title 'channel 08' with lines, \
	dataFile using ($0):($9 * 1 +0 ) title 'channel 09' with lines, \
	dataFile using ($0):($10 * 1 +0 ) title 'channel 10' with lines, \
	dataFile using ($0):($11 * 1 +0 ) title 'channel 11' with lines, \
	dataFile using ($0):($12 * 1 +0 ) title 'channel 12' with lines, \
	dataFile using ($0):($13 * 1 +0 ) title 'channel 13' with lines, \
	dataFile using ($0):($14 * 1 +0 ) title 'channel 14' with lines, \
	dataFile using ($0):($15 * 1 +0 ) title 'channel 15' with lines, \
	dataFile using ($0):($16 * 1 +0 ) title 'channel 16' with lines, \
	dataFile using ($0):($17 * 1 +0 ) title 'channel 17' with lines, \
	dataFile using ($0):($18 * 1 +0 ) title 'channel 18' with lines, \
	dataFile using ($0):($19 * 1 +0 ) title 'channel 19' with lines, \
	dataFile using ($0):($20 * 1 +0 ) title 'channel 20' with lines, \
	dataFile using ($0):($21 * 1 +0 ) title 'channel 21' with lines, \
	dataFile using ($0):($22 * 1 +0 ) title 'channel 22' with lines, \
	dataFile using ($0):($23 * 1 +0 ) title 'channel 23' with lines, \
	dataFile using ($0):($24 * 1 +0 ) title 'channel 24' with lines, \
	dataFile using ($0):($25 * 1 +0 ) title 'channel 25' with lines, \
	dataFile using ($0):($26 * 1 +0 ) title 'channel 26' with lines, \
	dataFile using ($0):($27 * 1 +0 ) title 'channel 27' with lines, \
	dataFile using ($0):($28 * 1 +0 ) title 'channel 28' with lines, \
	dataFile using ($0):($29 * 1 +0 ) title 'channel 29' with lines, \
	dataFile using ($0):($30 * 1 +0 ) title 'channel 30' with lines, \
	dataFile using ($0):($31 * 1 +0 ) title 'channel 31' with lines, \
	dataFile using ($0):($32 * 1 +0 ) title 'channel 32' with lines
	
 
