# Plot the first 8 channels of ATCA-MIMO-ISOL
#set terminal "jpeg"
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set output "/home/mmz/ATCA-MIMO-ISOL-plot_01.jpeg"
set xrange [0:19999]                
#set xrange [0:3999]                
#set xrange [0:399]                
#set xrange [0:50]                
#set xrange [0:199]                
#set x2range [0:599]                
#set x2range [0:4999]                
#set x2range [0:99]                
#set x2range [0:25]                
#set xtics  0,200,1199
#set yrange [-1500:1500] 
#set yrange [-32768:32767] 
set yrange [-35000:35000] 
#set yrange [-6000:6000] 
#set yrange [-14000:14000] 
#set yrange [-500:500] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
#set title "ATCA-MIMO-ISOL Board #14 -  Inputs tied to GND  Ch 1-4 integrators - 17.09.2015 MMZ IPP/GAR"
set title "ATCA-MIMO-ISOL #?? - Inputs 100Hz-3.4Vpp Fchop=2Khz Ch 1-4 integrators - 07.10.2015 MMZ IPP/HGW"
#set title "ATCA-MIMO-ISOL #14 - Inputs 200Hz-3.5Vpp Fchop=0Khz Ch 1-4 integrators - 18.09.2015 MMZ IPP/GAR"
dataFile='data.txt'
plot dataFile using ($0):($3 * 1) title 'channel 03' with lines, \
	dataFile using ($0):($4 * 1 +200 ) title 'channel 04' with lines, \
	dataFile using ($0):($5 * 1 +400 ) title 'channel 05' with lines, \
	dataFile using ($0):($6 * 1 +600 ) title 'channel 06' with lines, \
	dataFile using ($0):($7 * 1 +800 ) title 'channel 07' with lines, \
	dataFile using ($0):($8 * 1 +1000 ) title 'channel 08' with lines
	
 
