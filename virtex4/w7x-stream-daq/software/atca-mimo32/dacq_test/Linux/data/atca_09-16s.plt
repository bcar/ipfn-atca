# Plot channels 09 - 16 of ATCA-MIMO-ISOL
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set xrange [0:8191]                
set xrange [0:1199]                
set x2range [0:599]                
#set xtics  0,200,1199
set yrange [-32768:32767] 
#set yrange [-14000:14000] 
#set yrange [-115:90] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Test of IPP Board #14 - Fsignal = 5KHz-12Vpp - 06.08.2015 MMZ IPP/GAR"
dataFile='data_ch_9-16_5khz.txt'

plot dataFile using ($0):($9 * 1 +0 ) title 'channel 09' with lines, \
	dataFile using ($0):($10 * 1 +1000 ) title 'channel 10' with lines, \
	dataFile using ($0):($11 * 1 +2000 ) title 'channel 11' with lines, \
	dataFile using ($0):($12 * 1 +3000 ) title 'channel 12' with lines, \
	dataFile using ($0):($13 * 1 +4000 ) title 'channel 13' with lines, \
	dataFile using ($0):($14 * 1 +5000 ) title 'channel 14' with lines, \
	dataFile using ($0):($15 * 1 +6000 ) title 'channel 15' with lines, \
	dataFile using ($0):($16 * 1 +7000 ) title 'channel 16' with lines
	
 
