# Plot the first 8 channels of ATCA-MIMO-ISOL
#set terminal "jpeg"
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set output "/home/mmz/ATCA-MIMO-ISOL-plot_01.jpeg"
set term png enhanced font '/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf' 12
set output 'kian/atca_01-08-chop.png'

set xrange [0:19999]                
#set xrange [0:1199]                
#set xrange [0:50]                
#set xrange [0:199]                
#set x2range [0:599]                
set x2range [0:19999]                
#set x2range [0:99]                
#set x2range [0:25]                
#set xtics  0,200,1199
#set yrange [-1500:1500] 
#set yrange [-32768:32767] 
set yrange [-35000:35000] 
#set yrange [-6000:6000] 
#set yrange [-14000:14000] 
#set yrange [-200:200] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Board # - 100 Hz-3.4Vpp Ch 1-8 Integrators, 13.10.2015 MMZ IPP/HGW"
dataFile='data_01_08.txt'
plot dataFile using ($0):($1 * 1) title 'channel 01' with lines, \
	dataFile using ($0):($2 * 1 +0 ) title 'channel 02' with lines, \
	dataFile using ($0):($3 * 1 +0 ) title 'channel 03' with lines, \
	dataFile using ($0):($4 * 1 +0 ) title 'channel 04' with lines, \
	dataFile using ($0):($5 * 1 +0 ) title 'channel 05' with lines, \
	dataFile using ($0):($6 * 1 +0 ) title 'channel 06' with lines, \
	dataFile using ($0):($7 * 1 +0 ) title 'channel 07' with lines, \
	dataFile using ($0):($8 * 1 +0 ) title 'channel 08' with lines
	
set term x11
#set term wxt
replot
#pause -1 "Hit return to continue" 
