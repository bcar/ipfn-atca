# Plot channels 17 - 24 of ATCA-MIMO-ISOL
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set xrange [0:8191]                
set xrange [0:1199]                
set x2range [0:599]                
#set xtics  0,200,1199
set yrange [-32768:32767] 
#set yrange [-14000:14000] 
#set yrange [-115:90] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Test of IPP Board #14 - Fsignal = 5KHz-12Vpp - 06.08.2015 MMZ IPP/GAR"
dataFile='data.txt'

plot dataFile using ($0):($17 * 1 +0 ) title 'channel 17' with lines, \
	dataFile using ($0):($18 * 1 +1000 ) title 'channel 18' with lines, \
	dataFile using ($0):($19 * 1 +2000 ) title 'channel 19' with lines, \
	dataFile using ($0):($20 * 1 +3000 ) title 'channel 20' with lines, \
	dataFile using ($0):($21 * 1 +4000 ) title 'channel 21' with lines, \
	dataFile using ($0):($22 * 1 +5000 ) title 'channel 22' with lines, \
	dataFile using ($0):($23 * 1 +6000 ) title 'channel 23' with lines, \
	dataFile using ($0):($24 * 1 +7000 ) title 'channel 24' with lines
	
 
