# Plot the first 8 channels of ATCA-MIMO-ISOL
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set xrange [0:8191]                
set xrange [0:9999]                
#set xrange [0:1199]                
set x2range [0:599]                
#set xtics  0,200,1199
#set yrange [-32768:32767] 
set yrange [-7000:14000] 
#set yrange [-10:10]                    
#set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
set xtics format ""
set x2tics axis
set ytics axis
set title "ATCA-MIMO-ISOL Test of IPP Board #14 - Fsignal = 5KHz - 05.08.2015 MMZ IPP/GAR"
dataFile='data.txt'
plot dataFile using ($0):($1 * 1) title 'channel 01' with lines, \
	dataFile using ($0):($2 * 1 +1000) title 'channel 02' with lines, \
	dataFile using ($0):($3 * 1 +2000 ) title 'channel 03' with lines, \
	dataFile using ($0):($4 * 1 +3000 ) title 'channel 04' with lines, \
	dataFile using ($0):($5 * 1 +4000 ) title 'channel 05' with lines, \
	dataFile using ($0):($6 * 1 +5000 ) title 'channel 06' with lines, \
	dataFile using ($0):($7 * 1 +6000 ) title 'channel 07' with lines, \
	dataFile using ($0):($8 * 1 +7000 ) title 'channel 08' with lines
	
 
