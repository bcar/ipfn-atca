#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/types.h>
#include "atcaAdc_ioctl.h"
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "atcalib.h"

//Global vars
int run=1;						// main loop flag
int nraq=N_AQS, nrpk=N_PACKETS, acq=1;			// variables used to save data to the hard drive
short matData[DMA_SIZE/2]; 				// user space buffer for data
short acqData[N_AQS*N_PACKETS*NUM_CHAN];		// possibly larger buffer to store data to save to hard drive
unsigned int tmp; 					// generic
char errmsg[80];
int rc;							// generic
char *dev_name = "/dev/atcaAdcStream.14";
void sigint_handler(int sig);
void getkey (void);

int chopped_modules;
unsigned int chopper_freq;

int main(int argc, char **argv) {

	/* SIGINT */
	struct sigaction sa;
	sa.sa_handler = sigint_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, NULL);
	/* /SIGINT */

	/* TIMER config (optional) */
	struct itimerval timer;
	timer.it_interval.tv_sec = TIMERVALUE;
	timer.it_interval.tv_usec = 0;
	timer.it_value.tv_sec = TIMERVALUE;
	timer.it_value.tv_usec = 0;
	setitimer(ITIMER_REAL, &timer, NULL);
	/* /TIMER config */

	unsigned int loss_hits;	// counter for FPGA counter discontinuities (implies packet loss)
	unsigned int max_buffer_count;
	int i,  k;  					// loop iterators
	int fd; 					// file descriptor of device
	int hw_trigger = 0; 				// trigger scheme to start acquisition
	unsigned int oldtimer;				// value of last FPGA counter in buffer
	unsigned int old_loss_hits;
 	char *arg = "-sw";
 
	memset(&matData[0], 0, sizeof(matData));
	memset(&acqData[0], 0, sizeof(acqData));
	// uncomment if you want to save the first acquisitions to the disk
	enable_save_to_disk( &nrpk, &nraq, &acq);

	// Check command line
	if(argc < 2)
	{
        	printf("Usage: atcadaq -hw|-sw  [dev_name]\n");
		exit(-1);
	}

    	arg = argv[1];
    	if (!strcmp(arg, "-hw")) 
    	{
        	hw_trigger = 1; 
	}
	printf("arg1 = %s\n", arg);

	if(argc > 2)
	    dev_name = argv[2];

	printf("arg2 = %s\n", dev_name);

	chopped_modules = 1;	// Chopped modules are used
	// chopper_freq = 0 -> Chopper modules are operated NOT chopped!
	// chopper_freq is in Hz; e.g. 1000 = 1KHz	
	//chopper_freq = 1000; 	
	chopper_freq = 2000; 
	//chopper_freq = 0; 
	//chopper_freq = 4000; 	

	// Opening the device
	fd = init_device(dev_name, chopped_modules, chopper_freq);
	i = 0;
	oldtimer = 0;
	loss_hits = 0;
	old_loss_hits = 0;

	//Save Chopper signal in channel 32 (instead of ADC data)
	rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_CH32_ON);

	//Save NO Chopper signal in channel 32, but ADC data)
	//rc = ioctl(fd, PCIE_ATCA_IOCT_CHOP_CH32_OFF);

	// ADC board is armed and waiting for trigger (hardware or software)
	if (!hw_trigger)
	{
	  //		rc = ioctl(fd, PCIE_ADC_IOCT_STREAM_ENABLE);
	  //rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);
	  //	printf("FPGA Status: 0x%.8X\n", tmp);
		printf("software trigger mode active\n");
		getkey(); // Press any key to start the acquisition
		// Start the dacq with a software trigger
		rc = ioctl(fd, PCIE_ADC_IOCT_SOFT_TRIG); 
	        rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);
		printf("FPGA Status: 0x%.8X\n", tmp);
		// Check status to see if everything is okay (optional)
	}
	else
		printf("hardware trigger mode active\n");

	fflush(stdout);			

	while (run!=0) {
		oldtimer = concatenate(matData[DMA_SIZE/2-32],matData[DMA_SIZE/2-31]); // store previous counter to compare
		rc = 0;
		// note: while the following loop may look like it breaks the SIGINT way of gracefully exiting the application,
		// invoking a signal handler from inside a read system call exits the read with nonzero return, exiting the loop.
		// Of course there is still a very slim chance of breaking the intended behaviour if the SIGINT catches the 
		// process in the single instruction 'while (rc == 0)'
		while (rc == 0) {
			//printf("Waiting for dma data ...\n");
			rc = read(fd, &matData[0], DMA_SIZE); // loop until there is something to read.
			}
		i++;
		if ((concatenate(matData[0], matData[1])-oldtimer) != 1) { loss_hits++; }  // check to see if counter continuity is preserved across buffers (packet loss)
		
		//rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);	// Check status to see if buffer overflow has ocurred 
		// wait until we have gathered all the data we want to save
		if (((nrpk == N_PACKETS) && (acq == 1))||nrpk<N_PACKETS)
			// handle saving to the hard drive housekeeping
			save_to_disk(&nrpk, &nraq, &acq, &acqData[0], &matData[0]);

		//printf("bl:%i ", rc);

		if (i==25) {		// only update screen every 1000th read (printf uses a lot of syscalls)

			//printf("\n");				// carriage return, so updates are printed in the same line
			// the following for loop can be used to print several samples,
			// to check for instance if the FPGA counter is continuous inside the buffer.
			printf("S-1Ct %.8X ", oldtimer);
			for (k=0; k < 2; k++) {
				printf("S%iCt %.8X ", k+1, concatenate(matData[32*k],matData[32*k+1]));
				printf("S%iCn1 %.4hX ", k+1, matData[32*k+2]);
			}
			// print the last FPGA counter in the buffer.
			// Should be a multiple of 0x0800 times whatever is your iterator i limit
			// - in this case, it should be multiple of 1000 * 0x800 = 0x4000
			printf("LastCt %.8X ",concatenate(matData[DMA_SIZE/2-32],matData[DMA_SIZE/2-31])); 

			// divide the nr of packets lost by the nr of seconds the app has been running
			// to give a rough estimate of packet loss per second
			getitimer(ITIMER_REAL, &timer); 
			tmp = loss_hits/(TIMERVALUE - timer.it_value.tv_sec);
			printf("TotPL:%i PL:%i/%i AVGPL/s:%i  ", loss_hits,loss_hits-old_loss_hits,i, tmp);
			old_loss_hits = loss_hits;
			printf("\r");				// carriage return, so updates are printed in the same line
			// since we aren't printing any newline (\n), default linux behaviour
			// is to wait until stdout buffer is full before actually updating the screen.
			// We obviously don't want that so,
			fflush(stdout);			
			i = 0;
		}
	} /* End while */

	// this IOCTL returns the nr of times the driver IRQ handler was called
	// while there was still 1 or more buffers waiting to be read
	// Stop streaming and un-arm the FPGA.
	max_buffer_count = (unsigned int) ioctl(fd, PCIE_ADC_IOCT_ACQ_DISABLE);
	usleep(100);
	// must wait to call next IOCTL... FPGA PCIendpoint apparently handles race conditions poorly
	loss_hits = (unsigned int) ioctl(fd, PCIE_ADC_IOCT_STREAM_DISABLE);
	// and so, is useful but optional
	rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);						// Check status to see if everything is okay (optional)
	printf("Streaming off - FPGA Status: 0x%.8X %d mismatches %d max buffer count\n", tmp, loss_hits, max_buffer_count);

	tmp = close(fd); 
	if (tmp==-1) {
		printf("Error closing : %s\n", strerror(errno));
	}
  return 0;
}

void sigint_handler(int sig)
{
  run=0;
  printf("INT received!\n");
}

void
getkey ()
{
        printf("Press Return to continue ...\n");
        getchar();
}

