/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Device.cpp
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */

#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "atcaAdc_ioctl.h"
#include "ATCAMIMO32Device.h"

namespace atca
{

ATCAMIMO32Device::ATCAMIMO32Device(uint32_t bufferSize, bool useRTReadThread) :
                deviceName(""), deviceHandle(0), useRTReadThread(useRTReadThread),
                softTrigger(false), dmaBufferOffset(0), dmaBufferFillSize(0),
                dmaReadThread(0), dmaReadLoopActive(false)
{
    dmaBufferSize = bufferSize * 1024LL * 1024LL;
    dmaBuffer = (uint8_t*) malloc(dmaBufferSize);
    if (dmaBuffer == NULL)
        fprintf(stderr, "Failed to allocate memory (%llu bytes)\n",
                dmaBufferSize);
    sem_init(&readSemaphore, 0, 10);
    pthread_mutex_init(&readMutex, NULL);

#ifdef DEBUG
    readTimesFile = NULL;
#endif
}

ATCAMIMO32Device::~ATCAMIMO32Device()
{
    free(dmaBuffer);
    sem_destroy(&readSemaphore);
    pthread_mutex_destroy(&readMutex);
}

int ATCAMIMO32Device::open(string deviceName)
{
    this->deviceName = deviceName;
    errno = 0;
#ifdef DUMMYMODE
    deviceHandle = 0;
#else
    deviceHandle = ::open(deviceName.c_str(), O_RDONLY);
#endif

    if (deviceHandle == -1)
       return -1;
    return 0;
}

int ATCAMIMO32Device::close()
{
    int result = 0;
    errno = 0;
#ifndef DUMMYMODE
    result = ::close(deviceHandle);
#endif

    if (result == 0)
        deviceHandle = -1;

    return errno;
}

int ATCAMIMO32Device::setup(bool softTrigger, uint32_t readTimeout,
                            bool chopped, uint16_t chopperFreq)
{
    if (deviceHandle == -1)
        return EBADF;

    this->softTrigger = softTrigger;

    errno = 0;
#ifndef DUMMYMODE
    if (ioctl(deviceHandle, PCIE_ADC_IOCS_RDTMOUT, &readTimeout) != 0)
        return errno;
#endif
    if (chopped)
        return enableChopper(chopperFreq);
    else
        return disableChopper();
}

int ATCAMIMO32Device::disableChopper()
{
    errno = 0;
#ifndef DUMMYMODE
    ioctl(deviceHandle, PCIE_ATCA_IOCT_CHOP_OFF);
#endif
    return errno;
}

int ATCAMIMO32Device::enableChopper(uint16_t chopperFreq)
{
    errno = 0;

    //Set the Chop off as default
#ifdef DEBUG
    printf("Setting chopper off\n");
#endif
#ifndef DUMMYMODE
    if (ioctl(deviceHandle, PCIE_ATCA_IOCT_CHOP_OFF) != 0)
        return errno;
#endif

#ifdef DEBUG
    printf("Setting chopper signal level to 0 (default)\n");
#endif
    if (ioctl(deviceHandle, PCIE_ATCA_IOCT_CHOP_DEFAULT_0) != 0)
        return errno;

    unsigned int chop_period;
    if ((chopperFreq >= 100) && (chopperFreq <= 10000))
    {
        // Examples of chopperFreq and chop_period:

        // chopperFreq 10000 Hz -> chop_period = 200
        // chopperFreq  5000 Hz -> chop_period = 400
        // chopperFreq  4000 Hz -> chop_period = 500
        // chopperFreq  2000 Hz -> chop_period = 1000
        // chopperFreq  1000 Hz -> chop_period = 2000
        // chopperFreq   500 Hz -> chop_period = 4000

        // Calculate chop period;
        chop_period =
                2000000 / chopperFreq; // Fchopper = 2Khz - chop_period = 1000
#ifdef DEBUG
        printf("Setting Fchopper = %d Hz - chop_period = %d clks\n", chopperFreq, chop_period);
#endif
    }
    else
    {
        chop_period = 1000;
#ifdef DEBUG
        printf("Fchopper = %d Hz out of range (100Hz-10000Hz): Setting default Fchopper = %d Hz - chop_period = %d clks\n",
                chopperFreq, 2000, chop_period);
#endif
    }

    // Set the Chop's period, in this case is 1000 times the period of the acquisition period.
    // The Chop's frequency will be 2kHz

#ifndef DUMMYMODE
    unsigned int tmp = chop_period;
    if (ioctl(deviceHandle, PCIE_ATCA_IOCS_CHOP_MAX_COUNT, &tmp) != 0)
        return errno;
    tmp = chop_period / 2;
    if (ioctl(deviceHandle, PCIE_ATCA_IOCS_CHOP_CHANGE_COUNT, &tmp) != 0)
        return errno;
#endif

    // If "chopperFreq" is greater 0 then the module is operated in "chopped" mode.
    // Otherwise it is behaving like a "Standard module" with somewhat different properties (input stage)
    if (chopperFreq > 0)
    {
        //Set the Chop on
#ifdef DEBUG
        printf("Setting chopper on: ADC modules are operated in chopped mode\n");
#endif
#ifndef DUMMYMODE
        if (ioctl(deviceHandle, PCIE_ATCA_IOCT_CHOP_ON) != 0)
            return errno;
#endif
    }

    return 0;
}

int ATCAMIMO32Device::getSampleCounter(uint16_t* counter)
{
    if (deviceHandle == -1)
        return EBADF;

    errno = 0;
#ifndef DUMMYMODE
    ioctl(deviceHandle, PCIE_ADC_IOCG_COUNTER, counter);
#endif

    return errno;
}

int ATCAMIMO32Device::getStatus(uint16_t* status)
{
    if (deviceHandle == -1)
        return EBADF;

    errno = 0;
#ifndef DUMMYMODE
    ioctl(deviceHandle, PCIE_ADC_IOCG_STATUS, status);
#endif

    return errno;
}

#ifdef DEBUG
struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else
    {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}
#endif

void *ATCAMIMO32Device::dmaReadLoop(void* device)
{
#ifdef DEBUG
    struct timespec time, dt;
#endif

    ATCAMIMO32Device* dev = (ATCAMIMO32Device*) device;

    while (dev->dmaReadLoopActive)
    {
        if (dev->dmaBufferFillSize > dev->dmaBufferSize - DMA_SIZE)
        {
            // overflow error
            fprintf(stderr, "DMA BUFFER OVERFLOW - aborting DMA read loop\n");
            dev->dmaReadLoopActive = false;
        }
        else
        {
#ifdef DEBUG
            if (dev->readTimesFile != NULL)
            {
                clock_gettime(CLOCK_REALTIME, &time);
                dt = diff(dev->readTime, time);
                fprintf(dev->readTimesFile, "%d %d.%09d\n", dev->counter++, dt.tv_sec, dt.tv_nsec);
                dev->readTime = time;
            }
#endif

            pthread_mutex_lock(&dev->readMutex);
            uint64_t off = dev->dmaBufferOffset + dev->dmaBufferFillSize;
            pthread_mutex_unlock(&dev->readMutex);
            if (off >= dev->dmaBufferSize)
                off -= dev->dmaBufferSize;

#ifdef DUMMYMODE
            int i;
            uint16_t* tmp = (uint16_t*) (dev->dmaBuffer + off);
            for (i=0; i<DMA_SIZE/64; i++)
                tmp[i*32] = i;
            usleep(30000);
            int rc = DMA_SIZE;
#else
            int rc = ::read(dev->deviceHandle, dev->dmaBuffer + off, DMA_SIZE);
#endif
            if (rc != DMA_SIZE)
                fprintf(stderr,
                        "Failed to read DMA: rc=%d, bufferOffset=%llu, fillSize=%llu\n",
                        rc, dev->dmaBufferOffset, dev->dmaBufferFillSize);
            else
            {
                pthread_mutex_lock(&dev->readMutex);
                dev->dmaBufferFillSize += DMA_SIZE;
                pthread_mutex_unlock(&dev->readMutex);
                sem_post(&dev->readSemaphore);
            }
        }
    }
    sem_post(&dev->readSemaphore);
    dev->dmaReadThread = 0;
}

int ATCAMIMO32Device::createDMAReadThread()
{
    if (useRTReadThread)
    {
        struct sched_param param;
        pthread_attr_t attr;

        pthread_attr_init(&attr);
        pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        param.sched_priority = sched_get_priority_max(SCHED_FIFO);
        pthread_attr_setschedparam(&attr, &param);

        return pthread_create(&dmaReadThread, &attr, &ATCAMIMO32Device::dmaReadLoop, this);
    }

    return pthread_create(&dmaReadThread, NULL, &ATCAMIMO32Device::dmaReadLoop, this);
}

int ATCAMIMO32Device::enableAcquisition()
{
    if (deviceHandle == -1)
        return EBADF;

    errno = 0;
#ifndef DUMMYMODE
    if (ioctl(deviceHandle, PCIE_ADC_IOCT_ACQ_ENABLE) != 0)
        return errno;

    if (ioctl(deviceHandle, PCIE_ADC_IOCT_STREAM_ENABLE) != 0)
        return errno;

    if (softTrigger)
    {
        if (ioctl(deviceHandle, PCIE_ADC_IOCT_SOFT_TRIG) != 0)
            return errno;
    }
#endif

#ifdef DEBUG
    char filename[64];
    size_t n = deviceName.find_last_of('/');
    if (n == string::npos)
    snprintf(filename, 64, "dmaReadTimes_%s.txt", deviceName.c_str());
    else
    snprintf(filename, 64, "dmaReadTimes_%s.txt", deviceName.substr(n+1).c_str());
    readTimesFile = fopen(filename, "w");
    if (readTimesFile == NULL)
    fprintf(stderr, "Failed to open file to store DMA read times (%s): %s\n", filename, strerror(errno));
    else
    printf("Writing DMA readTimes to %s\n", filename);
    counter = 0;
    clock_gettime(CLOCK_REALTIME, &readTime);
#endif

    dmaBufferOffset = 0;
    dmaBufferFillSize = 0;

    dmaReadLoopActive = true;
    return createDMAReadThread();
}

int ATCAMIMO32Device::disableAcquisition()
{
    if (deviceHandle == -1)
        return EBADF;

    dmaReadLoopActive = false;

    if (dmaReadThread != 0)
    {
        int result = pthread_join(dmaReadThread, NULL);
        if (result != 0)
            fprintf(stderr, "Failed to join DMA read thread: %s", strerror(result));
    }

#ifndef DUMMYMODE
    // this IOCTL returns the nr of times the driver IRQ handler was called
    // while there was still 1 or more buffers waiting to be read
    // Stop streaming and un-arm the FPGA.
    unsigned int max_buffer_count = (unsigned int) ioctl(deviceHandle,
            PCIE_ADC_IOCT_ACQ_DISABLE);

    // must wait to call next IOCTL... FPGA PCIendpoint apparently handles race conditions poorly
    usleep(100);

    unsigned int loss_hits = (unsigned int) ioctl(deviceHandle,
            PCIE_ADC_IOCT_STREAM_DISABLE);
#endif

#ifdef DEBUG
    if (readTimesFile != NULL)
        fclose(readTimesFile);
#endif

    return 0;
}

int ATCAMIMO32Device::read(int16_t* buffer, uint8_t nChannels,
                           uint32_t nSamples)
{
    if (deviceHandle == -1)
        return EBADF;

    int offset = 0;
    int bytesToRead = nChannels << 1;
    int bufferSize = nSamples * nChannels;

#ifdef DEBUG
    fprintf(stderr, "DMA Buffer Level: %.02f\r", 100.0f * dmaBufferFillSize / dmaBufferSize);
#endif

    while (offset <= bufferSize - nChannels)
    {
        while (dmaReadLoopActive && (dmaBufferFillSize < bytesToRead))
            sem_wait(&readSemaphore);

        if (!dmaReadLoopActive && (dmaBufferFillSize < bytesToRead))
            return 0;

        pthread_mutex_lock(&readMutex);
        memcpy(buffer + offset, dmaBuffer + dmaBufferOffset, bytesToRead);
        dmaBufferOffset += NUM_CHAN << 1;
        if (dmaBufferOffset >= dmaBufferSize)
            dmaBufferOffset -= dmaBufferSize;
        dmaBufferFillSize -= NUM_CHAN << 1;
        pthread_mutex_unlock(&readMutex);
        offset += nChannels;
    }

    return nSamples;
}

} // namespace atca
