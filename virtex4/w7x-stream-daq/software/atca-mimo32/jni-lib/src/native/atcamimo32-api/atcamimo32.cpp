#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ATCAMIMO32Device.h"

using namespace std;
using namespace atca;

bool endReadLoop = false;

void printHelp(const char* programName)
{
    cout << endl;
    cout << "Usage: " << programName << " [<options>] <device>" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << "  -h\t\tshow this help" << endl;
    cout << "  -t\t\tenable hardware start trigger" << endl;
    cout << "  -b\t\tset the size of the internal data buffer in megabytes"
            << endl;
    cout << "  -r\t\tif specified the DMA read thread will be executed with real-time priority (requires root privileges)" << endl;
    cout << "  -s\t\tthe number of samples to read during one cycle" << endl;
    cout << "  -n\t\tthe number of channels to read" << endl;
    cout << "  -c\t\tenabled chopper" << endl;
    cout << "  -f\t\tset the chopper frequency (100..10000)" << endl;
    cout
            << "  -o\t\tspecifies the name of the file to write measured data values"
            << endl;
    cout << endl;
    cout
            << "<device>\tthe name of the ATCA MIMO32 device node (e.g. /dev/atcaAdcStream.3)"
            << endl;
    cout << endl;
}

void sigint_handler(int signo)
{
    endReadLoop = true;
}

int readDevice(string deviceName, uint32_t bufferSize, bool useRTReadThread,
               bool softTrigger, uint32_t readTimeout, uint32_t nSamples,
               uint8_t nChannels, bool chopped, uint16_t chopperFrequency,
               const char* outputFilename)
{
    ATCAMIMO32Device device(bufferSize, useRTReadThread);
    int rc = device.open(deviceName);
    if (rc != 0)
    {
        cerr << "Failed to open device '" << deviceName << "': " << strerror(rc)
                << endl;
        return EXIT_FAILURE;
    }

    rc = device.setup(softTrigger, readTimeout, chopped, chopperFrequency);
    if (rc != 0)
    {
        cerr << "Failed to setup device '" << deviceName << "': "
                << strerror(rc) << endl;
        device.close();
        return EXIT_FAILURE;
    }

    uint16_t status;
    rc = device.getStatus(&status);
    if (rc != 0)
        cerr << "Failed to read device status: " << strerror(rc) << endl;
    else
        printf("Device status: 0x%08x\n", status);

    FILE* outputFile = NULL;
    if (outputFilename != NULL)
    {
        outputFile = fopen(outputFilename, "w");
        if (outputFile == NULL)
            cerr << "Failed to open output file: " << strerror(errno) << endl;
    }

    int16_t* buffer = (int16_t*) malloc(2 * nSamples * nChannels);

    rc = device.enableAcquisition();
    if (rc != 0)
    {
        cerr << "Failed to enable acquisition: " << strerror(rc) << endl;
        device.close();
        return EXIT_FAILURE;
    }

    const uint64_t bufferByteSize = bufferSize * 1024LL * 1024LL;
    while (!endReadLoop)
    {
        rc = device.read(buffer, nChannels, nSamples);
        if (rc != nSamples)
        {
            cerr << "Failed to read device: return code = " << rc << endl;
            break;
        }

        float x = 40.0f * device.getBufferFillSize() / bufferByteSize;
        cout << "\rBuffer Fill Level: [";
        for (int i=0; i<40; i++)
            cout << (i<x ? '#' : ' ');
        cout << "]" << flush;

        if (outputFile != NULL)
        {
            int n = 0;
            for (int i = 0; i < nSamples; i++)
            {
                for (int j = 0; j < nChannels; j++)
                    fprintf(outputFile, "%d ", buffer[n++]);
                fprintf(outputFile, "\n");
            }
        }
    }
    cout << endl;

    rc = device.disableAcquisition();
    if (rc != 0)
        cerr << "Failed to disable acquisition: " << strerror(rc) << endl;

    free(buffer);
    device.close();

    if (outputFile != NULL)
        fclose(outputFile);

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[])
{
    uint32_t bufferSize = 128;
    bool softTrigger = true;
    bool useRTReadThread = false;
    uint32_t readTimeout = 5000;
    uint32_t nSamples = 200000;
    uint16_t nChannels = 32;
    bool chopped = false;
    uint16_t chopperFrequency = 2000;
    char* outputFilename = NULL;

    int opt;
    while ((opt = getopt(argc, argv, "hb:s:n:o:rtcf:")) != -1)
    {
        switch (opt)
        {
        case 'b':
            bufferSize = atoi(optarg);
            break;
        case 'r':
            useRTReadThread = true;
            break;
        case 's':
            nSamples = atoi(optarg);
            break;
        case 'n':
            nChannels = atoi(optarg);
            break;
        case 'o':
            outputFilename = optarg;
            break;
        case 't':
            softTrigger = false;
            break;
        case 'c':
            chopped = true;
            break;
        case 'f':
            chopperFrequency = atoi(optarg);
            break;
        case 'h':
        default:
            printHelp(argv[0]);
            return EXIT_SUCCESS;
        }
    }

    if (optind >= argc)
    {
        printHelp(argv[0]);
        return EXIT_FAILURE;
    }

    string deviceName(argv[optind++]);

    cout << "Buffer size:\t\t" << bufferSize << "MB" << endl;
    cout << "Number of samples:\t" << nSamples << endl;
    cout << "Number of channels:\t" << nChannels << endl;
    cout << "Software Trigger:\t" << (softTrigger ? "true" : "false") << endl;
    cout << "Chopper:\t\t" << (chopped ? "enabled" : "disabled") << endl;
    cout << "Chopper Frequency:\t" << chopperFrequency << endl;
    if (outputFilename != NULL)
        cout << "Output file:\t\t" << outputFilename << endl;
    cout << endl;

    struct sigaction sa;
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, NULL);

    return readDevice(deviceName, bufferSize, useRTReadThread, softTrigger, readTimeout,
            nSamples, nChannels, chopped, chopperFrequency, outputFilename);
}
