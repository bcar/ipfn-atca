/* ----------------------------------------------------------------------------
 * file: atcamimo32Jni.cpp
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <jniutils.h>
#include <stdint.h>

#include <ATCAMIMO32Device.h>
#include "de_mpg_ipp_codac_modules_atca_mimo32_ATCA_MIMO32.h"

using namespace atca;

#define LOGGER_NAME "de.mpg.ipp.codac.modules.atca.mimo32.ATCA_MIMO32"
#define EXCEPTION_NAME "java/lang/RuntimeException"

#define FINE(...) logMessage(env, LOGGER_NAME, JNI_FINE, __VA_ARGS__)
#define INFO(...) logMessage(env, LOGGER_NAME, JNI_INFO, __VA_ARGS__)
#define WARN(...) logMessage(env, LOGGER_NAME, JNI_WARNING, __VA_ARGS__)
#define THROW(...) throwException(env, EXCEPTION_NAME, __VA_ARGS__)

JNIEXPORT jlong JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_open(
        JNIEnv *env, jobject obj, jstring deviceName, jint bufferSize, jboolean useRTThread)
{
    ATCAMIMO32Device* device = new ATCAMIMO32Device(bufferSize, useRTThread == JNI_TRUE);

    const char* devName = env->GetStringUTFChars(deviceName, NULL);
    int result = device->open(devName);
    env->ReleaseStringUTFChars(deviceName, devName);

    if (result < 0)
    {
        WARN("[%s] Failed to open board: %s", device->getDeviceName().c_str(),
                strerror(errno));
        delete device;
        return -1;
    }

    INFO("[%s] board opened", device->getDeviceName().c_str());

    return reinterpret_cast<jlong>(device);
}

JNIEXPORT jint JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_close(
        JNIEnv *env, jobject obj, jlong deviceHandle)
{
    if (deviceHandle == 0)
        return 0;

    ATCAMIMO32Device* device = reinterpret_cast<ATCAMIMO32Device*>(deviceHandle);

    INFO("[%s] closing board", device->getDeviceName().c_str());

    int result = device->close();
    if (result < 0)
    {
        WARN("[%s] Failed to close board: %s", device->getDeviceName().c_str(),
                strerror(errno));
        return -1;
    }
    delete device;

    return 0;
}

JNIEXPORT jint JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_setup(
        JNIEnv *env, jobject obj, jlong deviceHandle, jboolean softTrigger,
        jint readTimeout, jboolean chopped, jint chopperFrequency)
{
    if (deviceHandle == 0)
        return 0;

    ATCAMIMO32Device* device = reinterpret_cast<ATCAMIMO32Device*>(deviceHandle);

    bool softTrig = softTrigger == JNI_TRUE ? true : false;
    bool chop = chopped == JNI_TRUE ? true : false;
    logMessage(env, LOGGER_NAME, JNI_INFO,
            "[%s] Initializing board (software trigger = %d, timeout = %d, chopped = %d, chopperFrequency = %d)",
            device->getDeviceName().c_str(), softTrig, readTimeout, chop,
            chopperFrequency);

    int result = device->setup(softTrig, readTimeout, chop, chopperFrequency);
    if (result != 0)
    {
        WARN("Failed to setup device: %s", strerror(result));
        return -1;
    }

    return 0;
}

JNIEXPORT jint JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_start(
        JNIEnv *env, jobject obj, jlong deviceHandle)
{
    if (deviceHandle == 0)
        return 0;

    ATCAMIMO32Device* device = reinterpret_cast<ATCAMIMO32Device*>(deviceHandle);

    INFO("[%s] Starting acquisition", device->getDeviceName().c_str());
    if (device->enableAcquisition() != 0)
    {
        WARN("[%s] Failed to enable acquisition: %s",
                device->getDeviceName().c_str(), strerror(errno));
        return -1;
    }

    uint16_t status;
    if (device->getStatus(&status) != 0)
    {
        WARN("[%s] Failed to get status: %s", device->getDeviceName().c_str(),
                strerror(errno));
        return -1;
    }

    uint16_t counter;
    if (device->getSampleCounter(&counter) != 0)
    {
        WARN("[%s] Failed to get sample counter: %s",
                device->getDeviceName().c_str(), strerror(errno));
        return -1;
    }

    FINE("[%s] Status: 0x%08X, Counter: 0x%08X",
            device->getDeviceName().c_str(), status, counter);

    return 0;
}

JNIEXPORT jint JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_stop(
        JNIEnv *env, jobject obj, jlong deviceHandle)
{
    if (deviceHandle == 0)
        return 0;

    ATCAMIMO32Device* device = reinterpret_cast<ATCAMIMO32Device*>(deviceHandle);

    INFO("[%s] Stopping acquisition", device->getDeviceName().c_str());
    if (device->disableAcquisition() != 0)
    {
        WARN("[%s] Failed to stop acquisition: %s",
                device->getDeviceName().c_str(), strerror(errno));
        return -1;
    }

    uint16_t status;
    if (device->getStatus(&status) != 0)
    {
        WARN("[%s] Failed to get status: %s", device->getDeviceName().c_str(),
                strerror(errno));
        return -1;
    }

    uint16_t counter;
    if (device->getSampleCounter(&counter) != 0)
    {
        WARN("[%s] Failed to get sample counter: %s",
                device->getDeviceName().c_str(), strerror(errno));
        return -1;
    }

    FINE("[%s] Status: 0x%08X, Counter: 0x%08X",
            device->getDeviceName().c_str(), status, counter);

    return 0;
}

JNIEXPORT jint JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_read(
        JNIEnv *env, jobject obj, jlong deviceHandle, jshortArray javaBuffer,
        jint numberOfChannels, jint numberOfSamples)
{
    if (deviceHandle == 0)
        return 0;

    int bufferSize = env->GetArrayLength(javaBuffer);
    if (bufferSize != numberOfChannels * numberOfSamples)
    {
        throwException(env, "java/lang/IllegalArgumentException",
                "Inconsistent arguments: bufferSize=%d, numberOfChannels=%d, numberOfSamples=%d",
                bufferSize, numberOfChannels, numberOfSamples);
        return -1;
    }

    ATCAMIMO32Device* device = reinterpret_cast<ATCAMIMO32Device*>(deviceHandle);

    int16_t* jdata = (int16_t*) env->GetPrimitiveArrayCritical(javaBuffer,
            NULL);
    if (jdata == NULL)
    {
        WARN("[%s] Failed to access java buffer",
                device->getDeviceName().c_str());
        return -1;
    }

    int rc = device->read(jdata, numberOfChannels, numberOfSamples);

    env->ReleasePrimitiveArrayCritical(javaBuffer, jdata, 0);

    if (rc < 0)
    {
        WARN("[%s] Failed to read data: %s", device->getDeviceName().c_str(),
                strerror(errno));
        return -1;
    }

    return rc;
}

JNIEXPORT jint JNICALL Java_de_mpg_ipp_codac_modules_atca_mimo32_ATCA_1MIMO32_getStatus(
        JNIEnv *env, jobject obj, jlong deviceHandle)
{
    if (deviceHandle == 0)
        return 0;

    ATCAMIMO32Device* device = reinterpret_cast<ATCAMIMO32Device*>(deviceHandle);

    uint16_t status;
    if (device->getStatus(&status) != 0)
    {
        WARN("[%s] Failed to get status: %s", device->getDeviceName().c_str(),
                strerror(errno));
        return -1;
    }

    return status;
}
