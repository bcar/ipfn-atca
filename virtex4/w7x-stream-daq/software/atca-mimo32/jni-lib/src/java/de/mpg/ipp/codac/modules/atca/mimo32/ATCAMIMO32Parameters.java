/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Parameters.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

/**
 * Interface definition the defines all parameters used by the {@link ATCAMIMO32Device}.
 */
public interface ATCAMIMO32Parameters {

    /**
     * Returns true if the software trigger should be used, false if not.
     */
    boolean isUseSoftTrigger();

    /**
     * Returns the read timeout in milliseconds.
     */
    int getReadTimeout();

    boolean isChopped();
    
    int getChopperFrequency();
    
}
