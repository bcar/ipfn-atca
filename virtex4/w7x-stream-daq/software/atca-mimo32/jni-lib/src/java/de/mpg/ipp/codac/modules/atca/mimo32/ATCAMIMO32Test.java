/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Test.java
 * project: dacqmodule-atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

/**
 * Main class to test ATCA functionality.
 */
public final class ATCAMIMO32Test {

    private static class TestParameters
        implements ATCAMIMO32Parameters {

        @Override
        public boolean isUseSoftTrigger() {
            return true;
        }

        @Override
        public int getReadTimeout() {
            return 5000;
        }

        @Override
        public boolean isChopped() {
            return false;
        }

        @Override
        public int getChopperFrequency() {
            return 1000;
        }

    }

    private static boolean stopped = false;

    private ATCAMIMO32Test() {}

    /**
     * Main method.
     * 
     * @param args
     *            command line arguments
     * @throws IOException
     *             if device access fails
     */
    public static void main( final String[] args ) {

        if (args.length < 1) {
            System.out.println("Usage: ATCAMIMO32Test <device name> [<nSamples> [<nChannels>]]");
            return;
        }

        final ATCAMIMO32Device device =
            new ATCAMIMO32Device(ATCAMIMO32NativeLibLoader.loadLibrary());
        if (!device.open(args[0], 256, false)) {
            System.err.println("Failed to open device");
            return;
        }

        device.setup(new TestParameters());

        final int nSamples = args.length > 1 ? Integer.parseInt(args[1]) : 65536;
        final int nChannels = args.length > 2 ? Integer.parseInt(args[2]) : 32;

        final short[] buffer = new short[nChannels * nSamples];

        if (!device.startDev()) {
            System.err.println("Failed to start device");
            device.close();
            return;
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                stopped = true;
                if (!device.stopDev())
                    System.err.println("Failed to stop device");
                else
                    System.out.println("Device stopped");

                if (!device.close())
                    System.err.println("Failed to close device");
                else
                    System.out.println("Device closed");
            }
        });

        short v = 0;
        while (!stopped) {
            final int samplesRead = device.readData(buffer, nChannels, nSamples);

            if (samplesRead <= 0)
                System.err.println("Failed to read: " + samplesRead);
            else {
                for (int i = 0; i < samplesRead; i++) {
                    /*System.out.print(i + ": ");
                    for (int j = 0; j < nChannels; j++)
                        System.out.printf("%04x ", buffer[i * nChannels + j]);
                    System.out.println();*/
                    final short tmp = buffer[i * nChannels];
                    if (tmp != Short.MIN_VALUE) {
                        final int diff = tmp - v;
                        if (Math.abs(diff) > 10)
                            System.err.printf("difference between samples to large: %d: %d - %d\n",
                                              i,
                                              v,
                                              tmp);
                    }
                    v = tmp;
                }
            }
        }

    }

}
