/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Native.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

// CSOFF: MethodName

/**
 * The low-level interface for ATCA MIMO32 boards. These methods are usually implemented by native
 * methods.
 */
public interface ATCAMIMO32Native {

    /**
     * Opens the board.
     * 
     * @param deviceName
     *            the device name of the board (e.g. /dev/atcaAdcStream.3)
     * @param bufferSize
     *            the size of the internal buffer that is used for DMA reads in megabytes
     * @return the device handle
     */
    long open( String deviceName, int bufferSize, boolean useRTThread );

    /**
     * Closes the board.
     * 
     * @param deviceHandle
     *            the device handle of the board
     * @return 0 on success, -1 on error
     */
    int close( long deviceHandle );

    /**
     * Initializes the board with the given parameters.
     * 
     * @param deviceHandle
     *            the device handle of the board
     * @param softTrigger
     *            true if soft trigger should be used, false if not
     * @param readTimeout
     *            the read timeout in milliseconds
     * @return 0 on success
     */
    int setup( long deviceHandle,
               boolean softTrigger,
               int readTimeout,
               boolean chopped,
               int chopperFrequency );

    /**
     * Starts data acquisition.
     * 
     * @param deviceHandle
     *            the device handle of the board
     * @return 0 on success
     */
    int start( long deviceHandle );

    /**
     * Stops the current data acquisition.
     * 
     * @param deviceHandle
     *            the device handle of the board
     * @return 0 on success
     */
    int stop( long deviceHandle );

    /**
     * Reads samples from the device.
     * 
     * @param deviceHandle
     *            the device handle of the board
     * @param data
     *            the buffer to be filled with sample values
     * @param numberOfChannels
     *            the number of channels to read
     * @param numberOfSamples
     *            the number of samples to read
     * @return 0 on success
     */
    int read( long deviceHandle, short[] data, int numberOfChannels, int numberOfSamples );

    int getStatus( long deviceHandle );

}
