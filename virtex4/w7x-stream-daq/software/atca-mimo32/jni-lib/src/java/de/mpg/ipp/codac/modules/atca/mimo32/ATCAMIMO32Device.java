/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Device.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

import java.util.logging.Logger;

/**
 * This class implements the DAQ functions for a ATCA MIMO 32 Channel ADC board from IST .
 */
public final class ATCAMIMO32Device {

    private static final Logger LOGGER = Logger.getLogger(ATCAMIMO32Device.class.getName());

    private final ATCAMIMO32Native nativeLibrary;

    private long deviceHandle;// device handle for board

    /**
     * Creates a new instance.
     * 
     * @param nativeLibrary
     *            the native library implementation to use
     */
    public ATCAMIMO32Device( final ATCAMIMO32Native nativeLibrary ) {
        this.nativeLibrary = nativeLibrary;
        this.deviceHandle = 0;// initialize file handle
    }

    /**
     * Opens the ATCA_MIMO32 board.
     * 
     * @param deviceName
     *            the device name of the board to open (e.g. /dev/atcaAdcStream.3)
     * @param bufferSize
     *            the size of the internal buffer used for DMA reads in megabytes
     * @return true on success, false on failure
     */
    public boolean open( final String deviceName,
                         final int bufferSize,
                         final boolean useRTThread ) {

        synchronized (this) {
            deviceHandle = nativeLibrary.open(deviceName, bufferSize, useRTThread);

            if (deviceHandle > 0) {
                LOGGER.info("ATCA_MIMO32 open Board " + deviceName + " done");
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Closes the ATCA_MIMO32 board.
     * 
     * @return true on success, false on failure
     */
    public boolean close() {
        final int result;
        synchronized (this) {
            if (deviceHandle <= 0)
                return true;

            result = nativeLibrary.close(deviceHandle);
            if (result == 0)
                deviceHandle = 0;
        }
        return result == 0;
    }

    /**
     * Initialize the ATCA_MIMO32 board. This method is called only once after startup.
     * 
     * @param parameters
     *            the parameters used for initialization
     */
    public void setup( final ATCAMIMO32Parameters parameters ) {
        synchronized (this) {
            if (deviceHandle <= 0)
                return;

            nativeLibrary.setup(deviceHandle,
                                parameters.isUseSoftTrigger(),
                                parameters.getReadTimeout(),
                                parameters.isChopped(),
                                parameters.getChopperFrequency());
            // make sure data acquisition is not running due to a previous abort
            nativeLibrary.stop(deviceHandle);
        }
    }

    /**
     * Reads samples from the device.
     * 
     * @param buffer
     *            the buffer to be filled with sample values - the length must be == numberOfSamples
     * @param numberOfChannels
     *            the number of channels to read
     * @param numberOfSamples
     *            the number of samples to read
     * @return the number of samples that have been read
     */
    public int readData( final short[] buffer,
                         final int numberOfChannels,
                         final int numberOfSamples ) {
        final int result;

        synchronized (this) {
            if (deviceHandle <= 0)
                return 0;
            result = nativeLibrary.read(deviceHandle, buffer, numberOfChannels, numberOfSamples);
        }
        if (result < 0)
            return 0;
        return result;
    }

    /**
     * Starts continuous sampling.
     * 
     * @return true on success, false on failure
     */
    public boolean startDev() {
        synchronized (this) {
            if (deviceHandle <= 0)
                return false;

            LOGGER.fine("ATCA_MIMO32 Starting a Continuous Daq");
            return nativeLibrary.start(deviceHandle) == 0;
        }
    }

    /**
     * Stops current DAQ operation and reinitialize the DAQ circuity.
     * 
     * @return true on success, false on failure
     */
    public boolean stopDev() {
        synchronized (this) {
            if (deviceHandle <= 0)
                return false;

            LOGGER.fine("ATCA_MIMO32 Stop Acquiring Data ");
            return nativeLibrary.stop(deviceHandle) == 0;
        }
    }

    public int getStatus() {
        int status;
        synchronized (this) {
            if (deviceHandle <= 0)
                return 0;

            int retryCount = 0;
            do {
                status = nativeLibrary.getStatus(deviceHandle);
            } while ((status == 0xFFFFFFFF) && (retryCount++ < 3));
        }
        return status;
    }
}
