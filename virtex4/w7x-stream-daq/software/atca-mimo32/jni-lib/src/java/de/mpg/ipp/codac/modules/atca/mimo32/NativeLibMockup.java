/* ----------------------------------------------------------------------------
 * file: NativeLibMockup.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

import java.util.HashMap;
import java.util.Map;

// CSOFF: MethodName

/**
 * Mockup implementation for {@link ATCAMIMO32Native}.
 */
final class NativeLibMockup
    implements ATCAMIMO32Native {

    private final Map<Long, String> openedBoards = new HashMap<Long, String>();

    @Override
    public long open( final String deviceName, final int bufferSize, final boolean useRTThread ) {
        if (openedBoards.containsValue(deviceName))
            throw new AssertionError(String.format("device %s already opened", deviceName));

        final long deviceHandle = deviceName.hashCode();
        openedBoards.put(deviceHandle, deviceName);
        return deviceHandle;
    }

    @Override
    public int close( final long deviceHandle ) {
        if (!openedBoards.containsKey(deviceHandle))
            throw new AssertionError(String.format("device was not opened (handle=%d)",
                                                   deviceHandle));

        openedBoards.remove(deviceHandle);
        return 0;
    }

    @Override
    public int setup( final long deviceHandle,
                      final boolean softTrigger,
                      final int readTimeout,
                      final boolean chopped,
                      final int chopperFrequency ) {

        if (!openedBoards.containsKey(deviceHandle))
            throw new AssertionError(String.format("device was not opened (handle=%d)",
                                                   deviceHandle));
        return 0;
    }

    @Override
    public int start( final long deviceHandle ) {
        if (!openedBoards.containsKey(deviceHandle))
            throw new AssertionError(String.format("device was not opened (handle=%d)",
                                                   deviceHandle));
        return 0;
    }

    @Override
    public int read( final long deviceHandle,
                     final short[] data,
                     final int numberOfChannels,
                     final int numberOfSamples ) {
        if (!openedBoards.containsKey(deviceHandle))
            throw new AssertionError(String.format("device was not opened (handle=%d)",
                                                   deviceHandle));
        return 0;
    }

    @Override
    public int stop( final long deviceHandle ) {
        if (!openedBoards.containsKey(deviceHandle))
            throw new AssertionError(String.format("device was not opened (handle=%d)",
                                                   deviceHandle));
        return 0;
    }

    @Override
    public int getStatus( final long deviceHandle ) {
        // TODO Auto-generated method stub
        return 0;
    }

}
