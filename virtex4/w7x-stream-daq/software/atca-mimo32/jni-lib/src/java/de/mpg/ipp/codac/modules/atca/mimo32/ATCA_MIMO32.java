/* ----------------------------------------------------------------------------
 * file: ATCA_MIMO32.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.mpg.ipp.codac.util.info.JarResourceHandler;

// CSOFF: MethodName

/**
 * Java native functions declarations needed to access the shared library.
 */
final class ATCA_MIMO32
    implements ATCAMIMO32Native {

    private static final Logger logger = Logger.getLogger(ATCA_MIMO32.class.getName());

    static {
        try {
            JarResourceHandler.loadLibrary("atcamimo32Jni");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to load native library", e);
        }
    }

    @Override
    public native long open( final String deviceName,
                             final int bufferSize,
                             final boolean useRTThread );

    @Override
    public native int close( final long deviceHandle );

    @Override
    public native int setup( final long deviceHandle,
                             final boolean softTrigger,
                             final int readTimeout,
                             final boolean chopped,
                             final int chopperFrequency );

    @Override
    public native int start( final long deviceHandle );

    @Override
    public native int read( final long deviceHandle,
                            final short[] data,
                            final int numberOfChannels,
                            final int numberOfSamples );

    @Override
    public native int stop( final long deviceHandle );

    @Override
    public native int getStatus( final long deviceHandle );

}
