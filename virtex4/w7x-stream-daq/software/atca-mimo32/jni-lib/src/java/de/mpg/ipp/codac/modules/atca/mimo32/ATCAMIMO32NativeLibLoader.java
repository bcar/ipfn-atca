/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32NativeLibLoader.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;


/**
 * Loader class that handles {@link ATCAMIMO32Native} implementations and loads native libraries
 * when necessary.
 */
public final class ATCAMIMO32NativeLibLoader {


    private ATCAMIMO32NativeLibLoader() {}

    /**
     * Default method to be used in productive environments. The method will load the native library
     * and return a corresponding {@link ATCAMIMO32Native} implementation if loading of the native
     * library succeeded. If the native library could not be loaded <code>null</code> will be
     * returned.
     */
    public static ATCAMIMO32Native loadLibrary() {
        return new ATCA_MIMO32();
    }

    /**
     * Returns a mockup implementation that does not depend on native code for test purposes.
     */
    public static ATCAMIMO32Native getMockupLibrary() {
        return new NativeLibMockup();
    }

}
