/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Constants.java
 * project: atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.modules.atca.mimo32;

/**
 * Utility class that provides device specific constants.
 */
public final class ATCAMIMO32Constants {

    public static final int MAX_BUFFER_SIZE = 4096 * 1024 * 16; // Max. 64
                                                                // MWords kernel
                                                                // memory buffer
                                                                // per board
    public static final float SAMPLE_RATE = 2000000.0f; // This device has a
                                                        // fixed 2 MHz sample
                                                        // rate
    public static final int MAX_CHANNELS = 32; // This device has always 32
                                               // channels enabled

    public static final int INTERNAL = 0; // Signal is internally generated
    public static final int EXTERNAL = 1; // Signal comes from external source
    public static final int EDGE_FALLING = 0; // source
    public static final int RSE_INPUT = 0; // RSE signal input
    public static final int NRSE_INPUT = 1; // Non RSE signal input
    public static final int DIFF_INPUT = 2; // Differential signal input

    public static final int SINGLE_SHOT_MODE = 0; // Single Shot
    public static final int CONTINOUS_MODE = 1; // Continuous DACQ
    public static final int AD_B_10_V = 1; // Input Range = +/- 10 Volt

    /* We don't want to allow instances of this class. */
    private ATCAMIMO32Constants() {
        throw new UnsupportedOperationException();
    }
}
