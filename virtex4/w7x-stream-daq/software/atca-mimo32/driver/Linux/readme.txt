/* 
 * Readme for the ATCA MIMO 32-channel ADC board device driver installation
 * Last changes: May 23, 2016
 */
 
May  2016:  channel 32 can now be used to store ADC data (usual mode) or the chopper signal when Integrator ADC are used 
			to implement this feature two additional ioctl's were introduced:
				ioctl(fd, PCIE_ATCA_IOCT_CHOP_CH32_ON);
				ioctl(fd, PCIE_ATCA_IOCT_CHOP_CH32_OFF);

Oct. 2015:  device driver now supports both "Standard ADC Modules" and "Chopped ADC Modules"

The value rc when reading the Status Register of the Board e.g.  rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);
returns now the number of full buffers waiting for read in the circular ringbuffer. The maximum value can be 16
because the ringbuffer has 16 DMA buffer  entries.


The device driver now supports the "UDEV" device driver model and therefore needs sysfs support from the kernel.
See /etc/fstab for details.

To compile the device driver kernel module for the ATCA MIMO 32-channel ADC board
run "make" without any arguments. 
When the kernel module was compiled successfully a new version of the file
atcaAdcStream.ko is generated. 
To install the module on the system issue "make install" (needs root privileges)
and atcaAdcStream.ko is loaded into the kernel and the necessary devices nodes in /dev are generated dynamically.

The device is mapped under "/sys/class/atcaAdcStream/atcaAdcStream.5"

sudo /sbin/udevadm info -a -p /sys/class/atcaAdcStream

ATCA physical slot  1 has device node /dev/atcaAdcStream.10
ATCA physical slot  5 has device node /dev/atcaAdcStream.5
ATCA physical slot 12 has device node /dev/atcaAdcStream.13

"lsmod | grep atcaAdcStream" shows if the module was loaded successfully.

ls -l /dev/atcaAdc*
should show the following:
crw-rw-rw- 1 root root 250, 5 2013-08-21 17:01 /dev/atcaAdcStream.5

Note: Major device number is now not fixed anymore and can vary on different systems.

Faulty insertion of the module can be further checked by the execution of "dmesg" 

To deinstall/remove the kernel module and the device nodes from the system issue "make uninstall"
