/*
 * atcaAdc
 * Linux Device Driver
 *
 * SVN keywords
 * $Date: 2010-08-06 20:07:31 +0100 (Fri, 06 Aug 2010) $
 * $Revision: 1569 $
 * $URL: http://cdaq.ipfn.ist.utl.pt/svn/cdaq/ATCA/ATCA-IO-CONTROL/SoftwareBBC/atcaAdcStream/module/main.c $
 *


 * Last Revision: 05.04.2012 by mmz
 * Replaced  init_MUTEX(&pciDev->open_sem) by sema_init(&pciDev->open_sem,1) in Kernel versions > 2.6.32
 * 			because init_MUTEX is marked deprecated and not longer supported in newer kernels
 * Replaced  int  pcieAdc_ioctl (struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg){
 *   by      long pcieAdc_unlocked_ioctl ( struct file *filp, unsigned int cmd, unsigned long arg){
 *   because ioctl is marked deprecated and not longer supported in newer kernels >=  2.6.36
 * Replaced  pciDev = container_of(inode->i_cdev, PCIE_DEV, cdev); by  pciDev = (PCIE_DEV*)filp->private_data; because unlocked_ioctl () has no struct *inode parm
 *
 * Last Revision: 11.07.2013 by mmz
 * Replaced macro "SPIN_LOCK_UNLOCKED" by function "spin_lock_init(&splock)" for initialization of splock
 *
 * Last Revision: 18.08.2013 by mmz
 * Adapted to the UDEV device model
 *
 * Last Revision: 12.09.2015 by mmz
 * Unified version for Standard ADCs & Chopped ADCs (Integrators)
 * Chopper signal is (hard-)wired in FPGA to ADC channel #32 :10.10.2015 by bcar
 *
 * New feature: 23.05.2016 by mmz
 * Channel #32 can be selected now between Chopper signal or ADC data
*/


#include <linux/module.h>
#include <linux/pci.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/version.h>

#include <asm/uaccess.h>
#include <asm/msr.h>

#include <linux/wait.h>


#include "atcaAdc.h"
#include "atcaAdc_ioctl.h"

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,32)
   #ifndef init_MUTEX
   #define init_MUTEX(_m) sema_init(_m,1);
   #endif // #ifndef init_MUTEX#endif
#endif

/* UDEV changes */
int device_major = 0;  // Let the Linux kernel find a free major device number */
/* The number of available minor numbers */
#define MINOR_NUMBERS 10 // 0xffff

u32 data_loop; // inserted by mmz for test purposes

static struct pci_device_id ids[] = {
  { PCI_DEVICE(PCI_VENDOR_ID_XILINX, PCI_DEVICE_ID_FPGA) },
  { 0, },
};

MODULE_DEVICE_TABLE(pci, ids);


/*function prototypes*/
int pcieAdc_probe(struct pci_dev *pdev, const struct pci_device_id *id) ;
void pcieAdc_remove(struct pci_dev *pdev);

struct class *atcaAdcStream_class;

static struct pci_driver pcieAdc_pci = {
  .name = DEVICE_NAME,
  .id_table = ids,
  .probe = pcieAdc_probe,
  .remove = pcieAdc_remove
};

#define PCIE_READ32(addr)				ioread32(addr)
#define PCIE_WRITE32(value, addr)		iowrite32(value, addr)
#define PCIE_FLUSH32()					PCIE_READ32()


struct file_operations _fops;

/**
 * pcieAdc_open
 */
int pcieAdc_open(struct inode *inode, struct file *filp) {
  PCIE_DEV *pciDev;   /* device information */

  /** retrieve the device information  */

  pciDev = container_of(inode->i_cdev,  PCIE_DEV, cdev);

  if (down_interruptible(&pciDev->open_sem))
    return -ERESTARTSYS;
    
  filp->private_data = pciDev; //for other methods
  //atomic_set(&pciDev->rd_condition, 0); // prepare to read *****************************
  up(&pciDev->open_sem);    
  data_loop = 0; // inserted by mmz for test purposes
  return 0;

}

/**
 * pcieAdc_release
 *  		called by close
 */
int pcieAdc_release(struct inode *inode, struct file *filp) {
  PCIE_DEV *pciDev;   /* device information */

  
  /**
     retrieve the device information  */

  pciDev = container_of(inode->i_cdev,  PCIE_DEV, cdev);
  down(&pciDev->open_sem);
  filp->private_data = NULL;
  up(&pciDev->open_sem);

  return 0;
}

/**
 * pcieAdc_read
 */
ssize_t pcieAdc_read(struct file *filp, char *buf, size_t count, loff_t *f_pos) {
  u32* data;
  int _ret;
  ssize_t retval = 0;


  PCIE_DEV *pciDev = (PCIE_DEV *)filp->private_data; /* device information */

  //check if aligned, if not return error, also alloc memory
  if ((count % DMA_NBYTES))
    goto out;

  if(atomic_read(&pciDev->rd_condition) == 0)
  {
    if(wait_event_interruptible_timeout(pciDev->rd_q, atomic_read(&pciDev->rd_condition) !=0, 
				      pciDev->wt_tmout)==0)
	{
      		//printk(KERN_ALERT "atcaAdc read: wait_q timeout\n");
		goto out;
	}
  }

  /* Copy buffer in buf with copy_to_user */
  data = pciDev->dmaIO.buf[pciDev->curr_buf].addr_v;
  /* data[0] = data_loop++; /inserted by mmz for test purposes */
  _ret = copy_to_user(buf, data, count);
  if (_ret) {
    printk(KERN_DEBUG "atcaAdc_read: copy_to_user error:%d\n", _ret);
    return -EFAULT;
  }

  *f_pos += count;
  retval = count;

  atomic_dec(&pciDev->rd_condition);
  if (pciDev->curr_buf == (DMA_BUFFS-1))
	pciDev->curr_buf = 0;
  else
	pciDev->curr_buf++;
 out: 
  return retval;
}

/**
 * pcieAdc_write
 */
ssize_t pcieAdc_write(struct file *file, const char *buf, size_t count, loff_t * ppos) {
  printk(KERN_ERR"atcaAdc_write: not implemented\n");
  return 0;
}
/**
 * pcieAdc_ioctl

int pcieAdc_ioctl(struct inode *inode, struct file *filp,
		  unsigned int cmd, unsigned long arg){
*/
long pcieAdc_unlocked_ioctl( struct file *filp, unsigned int cmd, unsigned long arg){

  int err = 0, retval = 0;
  unsigned long  flags  = 0;
  u32 tmp; 
  COMMAND_REG    cReg;  
  PCIE_DEV *pciDev; /* for device information */
  STATUS_REG sReg;

  /* retrieve the device information  */

  // pciDev = container_of(inode->i_cdev, PCIE_DEV, cdev);
  pciDev = (PCIE_DEV*)filp->private_data;
  sReg.reg32 = ioread32( (void*) &pciDev->pHregs->status);

  printk(KERN_DEBUG "read  status Reg:0x%X, addr:0x%p, cmd: 0x%X\n", sReg.reg32, &pciDev->pHregs->status, cmd);

  

  /**
   * extract the type and number bitfields, and don't decode
   * wrong cmds: return ENOTTY (inappropriate ioctl) before access_ok()
   */
  if (_IOC_TYPE(cmd) != PCIE_ADC_IOC_MAGIC) return -ENOTTY;
  if (_IOC_NR(cmd) > PCIE_ADC_IOC_MAXNR) return -ENOTTY;

  /*
   * the direction is a bitmask, and VERIFY_WRITE catches R/W
   * transfers. `Type' is user-oriented, while
   * access_ok is kernel-oriented, so the concept of "read" and
   * "write" is reversed
   */
  if (_IOC_DIR(cmd) & _IOC_READ)
    err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
  else if (_IOC_DIR(cmd) & _IOC_WRITE)
    err =  !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
  if (err) return -EFAULT;
  switch(cmd) {

  case PCIE_ADC_IOCG_STATUS:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    //  ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    tmp = PCIE_READ32((void*) &pciDev->pHregs->status); 
    //  ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    if(copy_to_user((void __user *)arg, &tmp, sizeof(u32)))
      return -EFAULT;
    retval = pciDev->mismatches;  // New!
    break;

  case PCIE_ADC_IOCT_INT_ENABLE:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.ACQiE=1;
    cReg.cmdFlds.DMAiE=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;

  case PCIE_ADC_IOCT_INT_DISABLE:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.ACQiE=0;
    cReg.cmdFlds.DMAiE=0;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;
  case PCIE_ADC_IOCT_ACQ_ENABLE:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    pciDev->mismatches=0;
    pciDev->curr_buf=0;
    pciDev->max_buffer_count=0;
    atomic_set(&pciDev->rd_condition, 0);
    cReg.cmdFlds.ACQE=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;
  case PCIE_ADC_IOCT_ACQ_DISABLE : 
    retval = pciDev->max_buffer_count;
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.ACQE=0;
    cReg.cmdFlds.STRG=0;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
	
    break;
  case PCIE_ADC_IOCT_DMA_ENABLE:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.DMAE=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);

    break;
  case PCIE_ADC_IOCT_DMA_DISABLE : 
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.DMAE=0;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
	
    break;

  case PCIE_ADC_IOCG_COUNTER:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    //  ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    tmp = PCIE_READ32((void*) &pciDev->pHregs->hwcounter); 
    //  ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    if(copy_to_user((void __user *)arg, &tmp, sizeof(u32)))
      return -EFAULT;
    break;
  case  PCIE_ADC_IOCS_RDTMOUT:
    retval = __get_user(tmp, (int __user *)arg);
    if (!retval)
      pciDev->wt_tmout = tmp * HZ;
    break;
  case  PCIE_ADC_IOCS_RDOFF:
    retval = __get_user(tmp, (int __user *)arg);
    if (!retval){
      spin_lock_irqsave(&pciDev->irq_lock, flags);    
      //write dma OffSet
      PCIE_WRITE32(tmp, (void*) &pciDev->pHregs->dmaOffSet);    
      /* 		printk(KERN_DEBUG "atcaAdc ioctl dmaOffSet 0x%08X\n", */
      /* 			PCIE_READ32((void*) &pciDev->pHregs->dmaOffSet));     */
      spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    }
    break;
  case PCIE_ADC_IOCT_SOFT_TRIG:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.STRG=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;

  case PCIE_ADC_IOCT_CLOCKS_SHARED:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.ACQS=0;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;
  case PCIE_ADC_IOCT_CLOCKS_LOCAL:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.ACQS=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    printk(KERN_DEBUG "ioctl local clock  Status:0x%08x, Command 0x%08x\n",
	   PCIE_READ32((void*) &pciDev->pHregs->status),
	   PCIE_READ32((void*) &pciDev->pHregs->command));
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;

  case PCIE_ADC_IOCT_TRIG_SHARED:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.TRGS=0;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;
  case PCIE_ADC_IOCT_TRIG_LOCAL:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.TRGS=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    printk(KERN_DEBUG "ioctl local  Status:0x%08x, Command 0x%08x\n",
	   PCIE_READ32((void*) &pciDev->pHregs->status),
	   PCIE_READ32((void*) &pciDev->pHregs->command));

    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;
  case  PCIE_ADC_IOCS_TRIG_DLY:
    //set the trig delay
    retval = __get_user(tmp, (int __user *)arg);
    if (!retval){
      spin_lock_irqsave(&pciDev->irq_lock, flags);    
      //write 
      PCIE_WRITE32(tmp, (void*) &pciDev->pHregs->triggerDelay);    
      spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    }
    break;
  case PCIE_ADC_IOCG_TRIG_DLY:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    //  ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    tmp = PCIE_READ32((void*) &pciDev->pHregs->triggerDelay); 
    //  ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    if(copy_to_user((void __user *)arg, &tmp, sizeof(u32)))
      return -EFAULT;
    break;
  case PCIE_ADC_IOCT_STREAM_ENABLE:
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.STREAME=1;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
    break;
  case PCIE_ADC_IOCT_STREAM_DISABLE : 
    retval = pciDev->mismatches;
    spin_lock_irqsave(&pciDev->irq_lock, flags);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
    cReg.cmdFlds.STREAME=0;
    PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
    spin_unlock_irqrestore(&pciDev->irq_lock, flags);
	
    break;

  // Additional functions for chopper control
  case PCIE_ATCA_IOCT_CHOP_ON:
      spin_lock_irqsave(&pciDev->irq_lock, flags);

      cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
      cReg.cmdFlds.CHOP_ON=1;
      cReg.cmdFlds.CH32_CHOP=1;
      PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);

      spin_unlock_irqrestore(&pciDev->irq_lock, flags);

      break;

    case PCIE_ATCA_IOCT_CHOP_OFF:
      spin_lock_irqsave(&pciDev->irq_lock, flags);

      cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
      cReg.cmdFlds.CHOP_ON=0;
      cReg.cmdFlds.CH32_CHOP=0;
      PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);

      spin_unlock_irqrestore(&pciDev->irq_lock, flags);

      break;

    case PCIE_ATCA_IOCT_CHOP_DEFAULT_1:
      spin_lock_irqsave(&pciDev->irq_lock, flags);

      cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
      cReg.cmdFlds.CHOP_DEFAULT=1;
      PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);

      spin_unlock_irqrestore(&pciDev->irq_lock, flags);

      break;

    case PCIE_ATCA_IOCT_CHOP_DEFAULT_0:
      spin_lock_irqsave(&pciDev->irq_lock, flags);

      cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
      cReg.cmdFlds.CHOP_DEFAULT=0;
      PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);

      spin_unlock_irqrestore(&pciDev->irq_lock, flags);

      break;

    case  PCIE_ATCA_IOCS_CHOP_MAX_COUNT:
        if(copy_from_user(&tmp, (void __user *)arg, sizeof(tmp)))
          return -EFAULT;
        printk(KERN_DEBUG "PCIE_ATCA_IOCS_CHOP_MAX_COUNT,  Chop:%d", tmp);
        spin_lock_irqsave(&pciDev->irq_lock, flags);
        PCIE_WRITE32(tmp, (void*) &pciDev->pHregs->chop_max_count);
        spin_unlock_irqrestore(&pciDev->irq_lock, flags);

        break;

    case  PCIE_ATCA_IOCG_CHOP_MAX_COUNT:
        spin_lock_irqsave(&pciDev->irq_lock, flags);
        tmp=PCIE_READ32( (void*) &pciDev->pHregs->chop_max_count);
        spin_unlock_irqrestore(&pciDev->irq_lock, flags);
        printk(KERN_DEBUG "PCIE_ATCA_IOCG_CHOP_MAX_COUNT,  Chop:%d", tmp);

        if(copy_to_user((void __user *)arg, &tmp, sizeof(tmp)))
          return -EFAULT;

        break;

    case  PCIE_ATCA_IOCS_CHOP_CHANGE_COUNT:
        if(copy_from_user(&tmp, (void __user *)arg, sizeof(tmp)))
          return -EFAULT;
        printk(KERN_DEBUG "PCIE_ATCA_IOCS_CHANGE_COUNT,  Chop:%d", tmp);
        spin_lock_irqsave(&pciDev->irq_lock, flags);
        PCIE_WRITE32(tmp, (void*) &pciDev->pHregs->chop_change_count);
        spin_unlock_irqrestore(&pciDev->irq_lock, flags);

        break;

    case PCIE_ATCA_IOCT_CHOP_CH32_ON:
          spin_lock_irqsave(&pciDev->irq_lock, flags);

          cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
          cReg.cmdFlds.CH32_CHOP=1;
          PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
          spin_unlock_irqrestore(&pciDev->irq_lock, flags);

        break;

    case PCIE_ATCA_IOCT_CHOP_CH32_OFF:
          spin_lock_irqsave(&pciDev->irq_lock, flags);
          cReg.reg32=PCIE_READ32((void*) &pciDev->pHregs->command);
          cReg.cmdFlds.CH32_CHOP=0;
          PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
          spin_unlock_irqrestore(&pciDev->irq_lock, flags);

        break;

	
  default:  /* redundant, as cmd was checked against MAXNR */
    return -ENOTTY;
  }

  return retval;

}

struct file_operations _fops = {
 owner:   THIS_MODULE,
 read:    pcieAdc_read,
 write:   pcieAdc_write,
 unlocked_ioctl:   pcieAdc_unlocked_ioctl,
 open:    pcieAdc_open,
 release: pcieAdc_release,
};

/*
 * pcieAdc_handler
 */
static irqreturn_t pcieAdc_handler(int irq, void* dev_id) { 
  PCIE_DEV      *pciDev;
  unsigned long  flags;
  int tmp;
  irqreturn_t    _ret = IRQ_HANDLED;    

  pciDev = (PCIE_DEV *) pci_get_drvdata(dev_id);
  spin_lock_irqsave(&pciDev->irq_lock, flags);

  tmp = atomic_read(&pciDev->rd_condition);
  if (tmp > pciDev->max_buffer_count)
	pciDev->max_buffer_count = tmp;
  // if (tmp != 0)
  //pciDev->mismatches++;
  pciDev->mismatches = tmp;

  atomic_inc(&pciDev->rd_condition);

  if (waitqueue_active(&pciDev->rd_q)){
  	wake_up_interruptible(&pciDev->rd_q);
  }

  spin_unlock_irqrestore(&pciDev->irq_lock, flags);
  return _ret;
}

//****************************
//* DMA management functions *
//****************************/
int enableDMAonboard(struct pci_dev *pdev) {
  int _ret =0;

  _ret=pci_dma_supported(pdev,  DMA_BIT_MASK(32));
  if (! _ret ){
    printk(KERN_WARNING "atcaAdc_probe DMA not supported. EXIT\n");
    return _ret;
  }
  /* enabling DMA transfers */
  _ret = pci_set_dma_mask(pdev, DMA_BIT_MASK(32));
  if (_ret) {
    printk(KERN_DEBUG "atcaAdc_probe pci_set_dma_mask error(%d). EXIT\n", _ret);
    return _ret;
  }
  _ret = pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(32));
  if (_ret) {
    printk(KERN_DEBUG "atcaAdc_probe pci_set_consistent_dma_mask error(%d). EXIT\n", _ret);
    return _ret;
  }
  /* setting DMA mastering mode */
  (void) pci_set_master(pdev);
  _ret = pci_set_mwi(pdev);
  if (_ret) {
    printk(KERN_DEBUG "atcaAdc_probe pci_set_mwi error(%d). EXIT\n", _ret);
    return _ret;
  }
  return _ret;

}

int configurePCI(PCIE_DEV *pcieDev) {
  u16 reg16 = 0;
  int i = 0;
  int _ret = 0;


  //set command register
  pci_read_config_word(pcieDev->pdev, PCI_COMMAND, &reg16);
  reg16 &= ~PCI_COMMAND_IO; // disable IO port access
  reg16 |= PCI_COMMAND_PARITY; // enable parity error hangs
  reg16 |= PCI_COMMAND_SERR; // enable addr parity error
  pci_write_config_word(pcieDev->pdev, PCI_COMMAND, reg16);

  //PCI reading IO memory spaces and set virtual addresses
  for (i = 0; i < 2; i++) {
    pcieDev->memIO[i].start = pci_resource_start(pcieDev->pdev, i);
    pcieDev->memIO[i].end = pci_resource_end(pcieDev->pdev, i);
    pcieDev->memIO[i].len = pci_resource_len(pcieDev->pdev, i);
    pcieDev->memIO[i].flags = pci_resource_flags(pcieDev->pdev, i);
    // virtual addr
    pcieDev->memIO[i].vaddr = ioremap_nocache(pcieDev->memIO[i].start,
					      pcieDev->memIO[i].len);
    printk(KERN_DEBUG "atcaAdc_probe start 0x%X, end 0X%X, len 0x%X, flags 0x%X \n",
	   (int) pcieDev->memIO[i].start,
	   (int) pcieDev->memIO[i].end,
	   (int) pcieDev->memIO[i].len,
	   (int) pcieDev->memIO[i].flags);

    if (!pcieDev->memIO[i].vaddr) {
      printk(KERN_ERR "atcaAdc: error in ioremap_nocache [%d]. Aborting.\n", _ret);
      return -ENOMEM;
    }
  }
  //virtual pointer to board registers
  pcieDev->pHregs = (PCIE_HREGS *) pcieDev->memIO[1].vaddr;

  return _ret;
}

int setupDMA(PCIE_DEV *pcieDev) {
  int i = 0;
  /**
     setting DMA regions */
  pcieDev->dmaIO.buf_size = PAGE_SIZE * (1 << GFPORDER);// DMA_NBYTES;//
  iowrite32(pcieDev->dmaIO.buf_size, (void*) &pcieDev->pHregs->dmaNbytes);  // write the buffer size to the FPGA

  for( i=0; i<DMA_BUFFS; i++){
    // set up a coherent mapping through PCI subsystem

    pcieDev->dmaIO.buf[i].addr_v = pci_alloc_consistent(pcieDev->pdev, pcieDev->dmaIO.buf_size,
							&(pcieDev->dmaIO.buf[i].addr_hw));
    if (!pcieDev->dmaIO.buf[i].addr_v || !pcieDev->dmaIO.buf[i].addr_hw) {
      printk(KERN_DEBUG "atcaAdc_probe pci_alloc_consistent error(v:%p hw:%p). EXIT\n",
	     (void*)pcieDev->dmaIO.buf[i].addr_v, (void*)pcieDev->dmaIO.buf[i].addr_hw);
      return -ENOMEM;
    }
    memset((void*)(pcieDev->dmaIO.buf[i].addr_v), 0, pcieDev->dmaIO.buf_size);
  }

  for( i=0; i < DMA_BUFFS; i++) // WRITE pci MA registers
    iowrite32(pcieDev->dmaIO.buf[i].addr_hw, (void*) &pcieDev->pHregs->HwDmaAddr[i]);

  pcieDev->dmaIO.buf_actv=0;
  pcieDev->dmaIO.hw_actv=pcieDev->dmaIO.buf[0].addr_hw;

  return 0;
}

/*
 * probe
 */
int pcieAdc_probe(struct pci_dev *pdev, const struct pci_device_id *id) {
  int _ret;
  PCIE_DEV * pciDev = NULL;
  STATUS_REG  sReg;
  COMMAND_REG    cReg;
  u32   _minor;

  /* allocate the device instance block */
  pciDev = kzalloc(sizeof(PCIE_DEV), GFP_KERNEL);
  if (!pciDev) {
    return -ENOMEM;
  }
  pciDev->pdev = pdev;
  pciDev->wt_tmout = 5 * HZ; /*time out in sec*/
  pci_set_drvdata(pdev, pciDev);
  init_MUTEX(&pciDev->open_sem);
  // sema_init(&pciDev->open_sem,1);

  /* enabling PCI board */
  _ret = pci_enable_device(pdev);
  if (_ret) {
    printk(KERN_DEBUG "atcaAdc_probe pci_enable_device error(%d). EXIT\n", _ret);
    return _ret;
  }
  //enable DMA transfers
  _ret = enableDMAonboard(pdev);
  if (_ret != 0) {
    printk(KERN_ERR "atcaAdc: error in DMA initialization. Aborting.\n");
    return _ret;
  }

  // configure PCI and remap I/O
  _ret = configurePCI(pciDev);
  if (_ret != 0) {
    printk("KERN_ERR atcaAdc: error in PCI configuration. Aborting.\n");
    return _ret;
  }
  //Set up DMA
  _ret = setupDMA(pciDev);
  if (_ret != 0) {
    printk("KERN_ERR atcaAdc: error in DMA setup. Aborting.\n");
    return _ret;
  }
  sReg.reg32 = ioread32( (void*) &pciDev->pHregs->status);
  printk(KERN_DEBUG "read  status Reg:0x%X, revId: 0x%x, statWrd: 0x%x, Master: %d, slotID:%d\n",
	 sReg.reg32, sReg.Str.revId, sReg.Str.statWrd, sReg.statFlds.MASTER, sReg.statFlds.slotID);
  printk(KERN_DEBUG "atcaAdc_probe Bar 1 Command 0x%08x\n",
	 ioread32((void*) &pciDev->pHregs->command));
  printk(KERN_DEBUG "atcaAdc_probe Bar 1 DMA_SIZE_NUMB 0x%08x\n",
	 ioread32((void*) &pciDev->pHregs->dmaReg));

  printk(KERN_DEBUG "atcaAdc_probe Bar DMA_CURR_BUFF 0x%08x\n",
	 ioread32((void*) &pciDev->pHregs->dmaCurrBuff));

  printk(KERN_DEBUG "atcaAdc_probe Bar 0 address 0 0x%08x\n",
	 ioread32(pciDev->memIO[0].vaddr));
	
  // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
  /* Install board IRQ */

    _ret=pci_enable_msi(pdev);  

    if (_ret) {
      printk(KERN_WARNING "pci_enable_msi %d error[%d]\n", pdev->irq, _ret);
      return _ret;
    }
    pciDev->irq = pdev->irq;    

    _ret = request_irq(pdev->irq, pcieAdc_handler, IRQF_SHARED, DRV_NAME, (void*) pdev);

    if (_ret) {
      printk(KERN_WARNING "rt_request_linux_irq irq %d error[%d]\n", pdev->irq, _ret);
      return _ret;
    }
    printk(KERN_DEBUG "atcaAdc irq %d handler installed\n", pdev->irq);
  

   // pciDev->irq_lock = SPIN_LOCK_UNLOCKED;   /* SPIN_LOCK_UNLOCKED is deprecated */
   // MMZ: Use function spin_lock_init() instead of macro SPIN_LOCK_UNLOCKED for initilization
   spin_lock_init(&pciDev->irq_lock);

   init_waitqueue_head(&pciDev->rd_q);

   _minor = sReg.statFlds.slotID;				/* get minor device depending on (atca) slot id */
   pciDev->devno=MKDEV(device_major, _minor);

  /*
  _ret = register_chrdev_region(pciDev->devno, 1, pcieAdc_pci.name);
  if (_ret) {
    printk(KERN_ERR "probe register_chrdev_region failed : %d", _ret);
    return -EIO;
  }
  */

  cdev_init(&pciDev->cdev, &_fops);
  pciDev->cdev.owner = THIS_MODULE;
  pciDev->cdev.ops = &_fops;

  _ret = cdev_add(&pciDev->cdev,pciDev->devno,1);

  if (_ret < 0) {
    printk(KERN_NOTICE "Error %d adding pcieAdc device", _ret);
    return -EIO;
  }

  pciDev->dev= device_create(atcaAdcStream_class, NULL, pciDev->devno, NULL, "atcaAdcStream.%d", _minor);

  cReg.cmdFlds.ACQE=0;
  cReg.cmdFlds.STREAME=0;
  cReg.cmdFlds.CH32_CHOP=0; // Channel 32 has NO chopper as default
  PCIE_WRITE32(cReg.reg32, (void*) &pciDev->pHregs->command);
    
  sReg.reg32 = ioread32( (void*) &pciDev->pHregs->status);
  printk(KERN_DEBUG "read  status Reg:0x%X, addr:0x%p\n", sReg.reg32, &pciDev->pHregs->status);
  printk(KERN_DEBUG "pcieAdc_probe done ... Version %d.%d\n", 2,0);
  return 0;
}

/*
 * remove
 */
void pcieAdc_remove(struct pci_dev *pdev) {
  unsigned long flags;
  int i;
  PCIE_DEV * pciDev;

  /* get the device information data */
  pciDev = (PCIE_DEV *) pci_get_drvdata(pdev);
  if(pciDev->irq) {
    /* disable registered IRQ */
    //deregistering OS ISR and restore MSI
    free_irq(pciDev->irq, pdev);
    
    pci_disable_msi(pdev);
  }
  spin_lock_irqsave(&pciDev->irq_lock, flags);
  // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
  /*  Reset  Device */
  PCIE_WRITE32(0, (void*) &pciDev->pHregs->command );
  PCIE_WRITE32(0, (void*) &pciDev->pHregs->dmaReg );	/* WRITE pci MA register */
  // ----- ----- ----- ----- ----- ----- DEVICE SPECIFIC CODE ----- ----- ----- ----- ----- -----
  spin_unlock_irqrestore(&pciDev->irq_lock, flags);
  cdev_del(&pciDev->cdev);

  // unregister_chrdev_region(pciDev->devno,1);
  device_destroy(atcaAdcStream_class, pciDev->devno);   /* destroy the device created with device_create and remove from /sys/class vfs */

  /* deregistering DMAable areas and virtual addresses for the board */
  for( i=0; i<DMA_BUFFS; i++)
    pci_free_consistent(pdev, pciDev->dmaIO.buf_size,
			pciDev->dmaIO.buf[i].addr_v,
			pciDev->dmaIO.buf[i].addr_hw);
  for( i=0; i<2; i++)
    iounmap(pciDev->memIO[i].vaddr);
  
  /* disable PCI board */
  kfree(pciDev);
  pci_set_drvdata(pdev, NULL);
  pci_clear_mwi(pdev);
  pci_disable_device(pdev);
  printk(KERN_DEBUG "atcaAdc_remove removed. \n");
  return;
}


/*
 * pcieAdc_init
 */
static int __init pcieAdc_init(void) {
  int _ret;
  dev_t devno = 0;

  devno = MKDEV(0, 0);
  _ret = alloc_chrdev_region(&devno, 0, MINOR_NUMBERS, DEVICE_NAME);
  if (_ret) {
    printk ("Failed to register device %s with error %d\n", DEVICE_NAME, _ret);
    goto fail;
  }
  device_major = MAJOR(devno);
  printk(KERN_DEBUG "atca_init: device_num:%d\n", device_major);

  atcaAdcStream_class = class_create(THIS_MODULE, DRV_NAME);
  if (IS_ERR(atcaAdcStream_class)) {
    printk(KERN_ERR "Unable to allocate class\n");
    _ret = PTR_ERR(atcaAdcStream_class);
    //    return _ret;
    goto unreg_chrdev;
  }

  /* registering the board */
  _ret = pci_register_driver(&pcieAdc_pci);
  if (_ret) {
    printk(KERN_ALERT "atcaAdc_init pci_register_driver error(%d).\n", _ret);
    goto unreg_class;
    //    return _ret;
  }
  return _ret;
 unreg_class:
  	  class_unregister(atcaAdcStream_class);
  	  class_destroy(atcaAdcStream_class);
 unreg_chrdev:
  	  unregister_chrdev_region(MKDEV(device_major,0), MINOR_NUMBERS);
 fail:
 	 return _ret;

}

/*
 * pcieAdc_exit
 */
static void pcieAdc_exit(void) {
	/* unregistering the board */
	pci_unregister_driver(&pcieAdc_pci);
	class_unregister(atcaAdcStream_class);
	unregister_chrdev_region(MKDEV(device_major,0), MINOR_NUMBERS);
	return;
}


module_init(pcieAdc_init);
module_exit(pcieAdc_exit);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("Module for PCIe FPGA Endpoint");
MODULE_AUTHOR("Hugo Alves, Bernardo Carvalho, Antonio Barbalace, M. Zilker");
