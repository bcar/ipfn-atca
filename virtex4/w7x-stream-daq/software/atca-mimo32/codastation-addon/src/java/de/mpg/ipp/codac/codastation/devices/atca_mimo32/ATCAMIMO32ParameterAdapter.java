/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32ParameterAdapter.java
 * project: codastation-dacqmodule-atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.codastation.devices.atca_mimo32;

import de.mpg.ipp.codac.codastation.addon.Properties;
import de.mpg.ipp.codac.modules.atca.mimo32.ATCAMIMO32Parameters;

/**
 * Adapter class to map CoDaStation {@link Properties} to the {@link ATCAMIMO32Parameters}
 * interface.
 */
final class ATCAMIMO32ParameterAdapter
    implements ATCAMIMO32Parameters {

    static final String PROPERTY_USESOFTTRIGGER = "useSoftTrigger";
    static final String PROPERTY_READTIMEOUT = "readTimeout";
    static final String PROPERTY_ISCHOPPED = "isChopped";
    static final String PROPERTY_CHOPPERFREQUENCY = "chopperFrequency";

    private final Properties properties;

    /**
     * Creates a new instance.
     * 
     * @param properties
     *            the property instance to map
     */
    ATCAMIMO32ParameterAdapter( final Properties properties ) {
        if (properties == null)
            throw new NullPointerException("properties must not be null");
        this.properties = properties;
    }

    @Override
    public boolean isUseSoftTrigger() {
        return properties.getBool(PROPERTY_USESOFTTRIGGER, false);
    }

    @Override
    public int getReadTimeout() {
        return (int) properties.getLong(PROPERTY_READTIMEOUT, 5000);
    }

    @Override
    public boolean isChopped() {
        return properties.getBool(PROPERTY_ISCHOPPED, false);
    }

    @Override
    public int getChopperFrequency() {
        return (int) properties.getLong(PROPERTY_CHOPPERFREQUENCY, 1000);
    }

}
