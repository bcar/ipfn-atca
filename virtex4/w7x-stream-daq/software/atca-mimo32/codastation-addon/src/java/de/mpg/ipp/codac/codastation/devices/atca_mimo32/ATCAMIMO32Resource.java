/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32Resource.java
 * project: codastation-dacqmodule-atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.codastation.devices.atca_mimo32;

import static de.mpg.ipp.codac.codastation.buffer.SignalFormat.SCALAR_INT16;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.mpg.ipp.codac.codastation.addon.FeatureDescription.FeatureType;
import de.mpg.ipp.codac.codastation.addon.Properties;
import de.mpg.ipp.codac.codastation.addon.Resource;
import de.mpg.ipp.codac.codastation.addon.SignalProvider;
import de.mpg.ipp.codac.codastation.addon.SignalProvisionException;
import de.mpg.ipp.codac.codastation.buffer.BaseType;
import de.mpg.ipp.codac.codastation.buffer.BufferDescription;
import de.mpg.ipp.codac.codastation.buffer.StandardBufferDescription;
import de.mpg.ipp.codac.codastation.buffer.StandardSignalDescription;
import de.mpg.ipp.codac.codastation.buffer.WritableBuffer;
import de.mpg.ipp.codac.codastation.status.StationStatus;
import de.mpg.ipp.codac.modules.atca.mimo32.ATCAMIMO32Device;
import de.mpg.ipp.codac.modules.atca.mimo32.ATCAMIMO32Native;
import de.mpg.ipp.codac.modules.atca.mimo32.ATCAMIMO32NativeLibLoader;
import de.mpg.ipp.codac.modules.atca.mimo32.ATCAMIMO32Parameters;

/**
 * A {@link Resource} implementation that represents a ATCA MIMO32 ADC device.
 */
final class ATCAMIMO32Resource
    implements Resource, SignalProvider {

    public static final String PROPERTY_USE_MOCKUPLIB = "useMockupLibrary";
    public static final String PROPERTY_USE_RTTHREAD = "useRTThread";

    private static final Logger logger = Logger.getLogger(ATCAMIMO32Resource.class.getName());

    private final String deviceName;

    private ATCAMIMO32Device device;
    private int numberOfChannels;
    private int numberOfSamplesPerGet;

    /**
     * Creates a new resource.
     * 
     * @param deviceName
     *            the device name of the board (e.g. /dev/atcaAdcStream.3)
     */
    ATCAMIMO32Resource( final String deviceName ) {
        this.deviceName = deviceName;
        numberOfChannels = 32;
        numberOfSamplesPerGet = 1;
    }

    ///////////////////////////// Resource interface ///////////////////////////////////////

    @Override
    public Feature getFeature( final FeatureType type, final String specifier ) {
        if (FeatureType.SignalProvider.equals(type))
            return ATCAMIMO32AddOn.PROVIDER_SPECIFIER.equals(specifier) ? this : null;
        else
            return null;
    }

    @Override
    public void init( final StationStatus stationStatus, final Properties properties ) {
        final boolean useMockupLib =
            (properties != null) ? properties.getBool(PROPERTY_USE_MOCKUPLIB, false) : false;
        final boolean useRTThread =
            (properties != null) ? properties.getBool(PROPERTY_USE_RTTHREAD, false) : false;
        final ATCAMIMO32Native nativeLibrary =
            useMockupLib
                ? ATCAMIMO32NativeLibLoader.getMockupLibrary()
                : ATCAMIMO32NativeLibLoader.loadLibrary();

        if (nativeLibrary == null)
            return;

        device = new ATCAMIMO32Device(nativeLibrary);
        if (!device.open(deviceName, 256, useRTThread))
            logger.severe("Failed to open ATCA MIMO32 board " + deviceName);
        else {
            numberOfChannels = (int) properties.getLong("numberOfChannels", numberOfChannels);
            final ATCAMIMO32Parameters parameters =
                (properties != null) ? new ATCAMIMO32ParameterAdapter(properties) : null;
            device.setup(parameters);
        }
    }

    @Override
    public void dispose() {
        if (device == null)
            return;

        if (!device.close())
            logger.severe("Failed to close ATCA MIMO32 board " + deviceName);
    }

    @Override
    public Number getStateValue( final String specifier ) {
        if (device == null)
            return null;

        if (STATE_SPECIFIER_ERROR.equals(specifier))
            return 1;

        return null;
    }

    @Override
    public BufferDescription init( final int numberOfSamplesPerGet ) {
        if (device == null)
            throw new IllegalStateException("device not initialized");

        this.numberOfSamplesPerGet = numberOfSamplesPerGet;

        final StandardBufferDescription bufferDescription =
            new StandardBufferDescription(BaseType.Short, numberOfSamplesPerGet);
        final List<StandardSignalDescription> signalDescriptions =
            new ArrayList<StandardSignalDescription>();
        for (int i = 0; i < numberOfChannels; i++)
            signalDescriptions.add(new StandardSignalDescription(SCALAR_INT16));
        bufferDescription.addInterlacedSignals(0, signalDescriptions);

        if (!device.startDev())
            logger.severe("Failed to start ATCA MIMO32 board " + deviceName);

        return bufferDescription;
    }

    @Override
    public int get( final WritableBuffer buffer, final int timeout )
        throws SignalProvisionException {

        if (device == null)
            throw new SignalProvisionException("device not initialized", this);

        return device.readData((short[]) buffer.array(), numberOfChannels, numberOfSamplesPerGet);
    }

    @Override
    public void finish() {
        if (device == null)
            return;

        if (!device.stopDev())
            logger.severe("Failed to stop ATCA MIMO32 board " + deviceName);
    }

    @Override
    public long getAssumedDataRate() {
        // TODO Auto-generated method stub
        return 0;
    }

}
