/* ----------------------------------------------------------------------------
 * file: package-info.java
 * project: codastation-dacqmodule-atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */

/**
 * <h3>Provided add-on: {@link de.mpg.ipp.codac.codastation.devices.atca_mimo32.ATCAMIMO32AddOn ATCAMIMO32AddOn}</h3>
 * 
 * <h4>Provided resource:
 *   {@link de.mpg.ipp.codac.codastation.devices.atca_mimo32.ATCAMIMO32Resource ATCAMIMO32Resource}</h4>
 * 
 * <p>ATCAMIMO32Resource provides access to ATCA MIMO32 ADC devices.</p>
 * <dl>
 *   <dt><u>Allowed specifiers</u></dt>
 *   <dd>a number indicating the device - currently 0 or 1</dd>
 * </dl>
 * <dl>
 *   <dt><u>Properties</u></dt>
 *   <dd style="margin-top:1em">
 *     <table border="1">
 *       <tr><th>Name</th><th>Description</th><th>Type</th><th>Default</th></tr>
 *       <tr>
 *         <td>useMockupLibrary</td>
 *         <td>Enables/disables usage of a mockup library for low-level native device methods.</td>
 *         <td>boolean</td>
 *         <td>false</td>
 *       </tr>
 *     </table>
 *   </dd>
 * </dl>
 * 
 * <h5>Provided feature: Controller</h5>
 * 
 * <p>Interface to control the device.</p>
 * 
 * <dl>
 *   <dt><u>Feature Implementations</u></dt>
 *   <dd>Controllable</dd>
 * </dl>
 * <dl>
 *   <dt><u>Allowed specifiers</u></dt>
 *   <dd><code>control</code></dd>
 * </dl>
 * <dl>
 *   <dt><u>Properties</u></dt>
 *   <dd style="margin-top:1em">
 *     <table border="1">
 *       <tr><th>Name</th><th>Description</th><th>Type</th><th>Default</th></tr>
 *       <tr>
 *         <td>clockSource</td>
 *         <td>Input source of the sample clock; 0=internal, 1=external</td>
 *         <td>Integer</td>
 *         <td>0 (=internal)</td>
 *       </tr>
 *       <tr>
 *         <td>inputPolarity</td>
 *         <td>0=RSE, 1=NRSE, 2=DIFF</td>
 *         <td>Integer</td>
 *         <td>2 (=DIFF)</td>
 *       </tr>
 *       <tr>
 *         <td>inputRange</td>
 *         <td>1=AD_B_10_V</td>
 *         <td>Short</td>
 *         <td>1</td>
 *       </tr>
 *       <tr>
 *         <td>numberOfChannels</td>
 *         <td>The number of active input channels.</td>
 *         <td>Integer</td>
 *         <td>32</td>
 *       </tr>
 *       <tr>
 *         <td>resolution</td>
 *         <td>ADC resolution in bit</td>
 *         <td>Integer</td>
 *         <td>16</td>
 *       </tr>
 *       <tr>
 *         <td>sampleRate</td>
 *         <td>Sample frequency in Hz</td>
 *         <td>Float</td>
 *         <td>2000000</td>
 *       </tr>
 *       <tr>
 *         <td>startTriggerEdge</td>
 *         <td>Active edge of the start trigger</td>
 *         <td>Integer</td>
 *         <td>0 (=falling)</td>
 *       </tr>
 *       <tr>
 *         <td>startTriggerSource</td>
 *         <td>Input source of the start trigger; 0=internal, 1=external</td>
 *         <td>Integer</td>
 *         <td>0 (=internal)</td>
 *       </tr>
 *     </table>
 *   </dd>
 * </dl>
 * 
 * <h5>Provided feature: Signal Source</h5>
 * 
 * <p>Interface to retrieve acquired values.</p>
 * 
 * <dl>
 *   <dt><u>Feature Implementations</u></dt>
 *   <dd>SignalProvider</dd>
 * </dl>
 * <dl>
 *   <dt><u>Allowed specifiers</u></dt>
 *   <dd><code>source</code></dd>
 * </dl>
 * 
 * <h5>Provided feature: Flow Controller</h5>
 * 
 * <p>Interface to start and stop data acquisition.</p>
 * 
 * <dl>
 *   <dt><u>Feature Implementations</u></dt>
 *   <dd>SignalFlowController</dd>
 * </dl>
 * <dl>
 *   <dt><u>Allowed specifiers</u></dt>
 *   <dd><code>flow</code></dd>
 * </dl>
 * 
 * <h3>Dependencies</h3>
 * <ul>
 *   <li>
 *     DAQmx Java Handler
 *   </li>
 * </ul>
 * 
 * <h3>Related Documentation</h3>
 * <ul>
 *   <li><a href="http://codacwiki.ipp-hgw.mpg.de/projects/codastation/wiki">CoDaStation Wiki</a></li>
 * </ul>
 *
 * @see <a href="doc-files/codastation-dacqmodule-atca-mimo32.html">User Manual</a>
 */

package de.mpg.ipp.codac.codastation.devices.atca_mimo32;

