/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32AddOn.java
 * project: codastation-dacqmodule-atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.codastation.devices.atca_mimo32;

import java.util.Hashtable;
import java.util.Map;

import de.mpg.ipp.codac.codastation.addon.AddOn;
import de.mpg.ipp.codac.codastation.addon.FeatureDescription.FeatureType;
import de.mpg.ipp.codac.codastation.addon.Resource;
import de.mpg.ipp.codac.codastation.addon.ResourceDescription;
import de.mpg.ipp.codac.codastation.addon.StandardFeatureInfo;
import de.mpg.ipp.codac.codastation.addon.StandardResourceDescription;

/**
 * A CoDaStation add-on that provides {@link Resource} implementations to access ATCA-MIMO32 ADC
 * boards.
 */
public final class ATCAMIMO32AddOn
    implements AddOn {

    static final String PROVIDER_SPECIFIER = "source";

    private final ResourceDescription resourceInfo;
    private final Map<String, ATCAMIMO32Resource> resourceMap;

    /**
     * Creates a new instance.
     */
    public ATCAMIMO32AddOn() {
        resourceInfo = createResourceInfo();
        resourceMap = new Hashtable<String, ATCAMIMO32Resource>();
    }

    private static ResourceDescription createResourceInfo() {
        final StandardResourceDescription info = new StandardResourceDescription("\\w+", "", null);
        info.addFeature(new StandardFeatureInfo(PROVIDER_SPECIFIER, FeatureType.SignalProvider));
        return info;
    }

    @Override
    public ResourceDescription getResourceInfo() {
        return resourceInfo;
    }

    @Override
    public Resource getResource( final String specifier ) {
        if (resourceMap.containsKey(specifier))
            return resourceMap.get(specifier);

        final ATCAMIMO32Resource resource = new ATCAMIMO32Resource(specifier);
        resourceMap.put(specifier, resource);
        return resource;
    }

}
