/* ----------------------------------------------------------------------------
 * file: ATCAMIMO32AddOnTest.java
 * project: codastation-dacqmodule-atca-mimo32
 * by: Torsten Bluhm
 *
 * Copyright (c) 2015, All rights reserved.
 * Max-Planck-Institut für Plasmaphysik. W7-X CoDaC group.
 * ----------------------------------------------------------------------------
 */
package de.mpg.ipp.codac.codastation.devices.atca_mimo32;

import org.junit.Test;

import de.mpg.ipp.codac.codastation.addon.StandardProperties;
import de.mpg.ipp.codac.codastation.addonchecker.AddOnChecker;
import de.mpg.ipp.codac.codastation.testsuite.ResourceTestSuite;
import de.mpg.ipp.codac.codastation.testsuite.TestSuiteException;
import de.mpg.ipp.codac.codastation.testsuite.TestSuiteFactory;

/**
 * (J)unit tests for: MyAddOn.
 */
public final class ATCAMIMO32AddOnTest {

    /**
     * Runs standard checks for the add-on using the {@link AddOnChecker} tool.
     * 
     * @throws ClassNotFoundException
     *             if the add-on class is not known
     * @throws IllegalAccessException
     *             if the add-on class or its nullary constructor is not accessible.
     * @throws InstantiationException
     *             if the add-on can not be instantiated
     */
    @Test
    public void standardAddOnChecks()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException {

        final AddOnChecker addOnChecker = new AddOnChecker(ATCAMIMO32AddOn.class.getName());

        addOnChecker.checkSelfDescription();

        final StandardProperties resourceProperties = new StandardProperties();
        resourceProperties.set(ATCAMIMO32Resource.PROPERTY_USE_MOCKUPLIB, true);
        resourceProperties.set(ATCAMIMO32ParameterAdapter.PROPERTY_USESOFTTRIGGER, true);
        addOnChecker.checkResource("0", resourceProperties);
    }

    /**
     * Runs a simple integration test. A minimal CoDaStation will be set up using the defined
     * resources and features. A station state including the defined controllable properties will be
     * created and activated for the given time interval.
     * 
     * @throws TestSuiteException
     *             if some problem occurs running the test suite
     */
    @Test
    public void integrationTest()
        throws TestSuiteException {

        final StandardProperties resourceProperties = new StandardProperties();
        resourceProperties.set(ATCAMIMO32Resource.PROPERTY_USE_MOCKUPLIB, true);
        resourceProperties.set(ATCAMIMO32ParameterAdapter.PROPERTY_USESOFTTRIGGER, true);
        final ResourceTestSuite testSuite =
            TestSuiteFactory.createResourceTestSuite(ATCAMIMO32AddOn.class.getName(),
                                                     "/dev/atcaAdcStream.3",
                                                     resourceProperties);

        testSuite.runTest(5000);

        testSuite.shutdown();
    }

}
