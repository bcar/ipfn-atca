#ifndef PCIEADC_H_
#define PCIEADC_H_

#ifdef __KERNEL__
#include <linux/module.h>
#else
#define u32 unsigned int
#define dma_addr_t u32
#endif

/* board PCI id */
#define PCI_DEVICE_ID_FPGA	0x0020
#define PCIEADC_MAJOR 251

/* board configurable parameters */
//#define BAR1_RVAL 0xB
//  + (0x20 << 4)) // 256 Dwords = 1KB
// #define DMA_NBYTES PAGE_SIZE // 4096  Linux page size
#define DMA_NBYTES PAGE_SIZE // 128k 

#define DMA_BUFFS   16    //Number of DMA Buffs
#define GFPORDER 	10 // 2^5 = 32, 32*4k = 128k / gfpo 8 = 1Mb / gfpo 10 = 4 Mb
#define CHANNELS 	32

//#define CHAR_DEVICE_NAME      "atcaAdcStream"
#define DRV_NAME "atcaAdcStream"
#define DEVICE_NAME "atcaAdcStream"

#define MAX_BOARDS		 12

typedef struct _BAR_STRUCT {
	unsigned long start;
	unsigned long end;
	unsigned long len;
	unsigned long flags;
	void* vaddr;
} BAR_STRUCT;


typedef struct _DMA_BUF {
	void* addr_v;
	dma_addr_t addr_hw;
} DMA_BUF;

typedef struct _DMA_STRUCT {
  unsigned int buf_size;
  unsigned int buf_actv;
  dma_addr_t hw_actv;
  u32 *  daddr_actv; // ptr to data on active buffer
  //void*  vaddr_idle;
  //unsigned int buf_idle;
  DMA_BUF buf[DMA_BUFFS];
} DMA_STRUCT;

typedef struct _READ_BUF {
	int count;
	int total;
	u32* buf;				// assume that ADC data is 32bit wide
} READ_BUF;

//TOD : to be used.
#ifdef __BIG_ENDIAN_BTFLD
 #define BTFLD(a,b) b,a
#else
 #define BTFLD(a,b) a,b
#endif

//Not realy much use, since I/O must be done through ioread, iowrite etc..
typedef struct _PCIEADC_REG {
	u32 BTFLD(BTFLD(BTFLD(BTFLD(counter_en:1, int_mode:1), int_status:1), DMA_en:1), reserved:28);
	u32 DMA_addr;
	u32 DMA_size;
	u32 BTFLD(output:1, _reserved:31);
} PCIEADC_REG;

/*
 * 8 + 24 bit field 
 */
typedef struct _REVID_FLDS {
  u32 RevId:4, TMR:1, HDR:1, DBG:1, reserved:1, none:24;
} REVID_FLDS;

/*
 * 24 + 8 bit field
 */
typedef struct _STATUS_FLDS {
  u32 none:8, rsv0:8, 
    slotID:4, rsv2:2, MASTER:1, ERR0:1,  //rsv1:2, FSH:1, RST:1
    rsv3:2,  FIFE:1, FIFF:1, rsv4:2, DMAC:1, ACQC:1; // msb
} STATUS_FLDS;

typedef struct _STATUS_REG {
  union {
    u32 reg32;
    struct _STALFD {
      u32 revId:8,
	statWrd:24;
    } Str;
    STATUS_FLDS statFlds;
    REVID_FLDS rdIdFlds;
  };
} STATUS_REG;

typedef struct _COMMAND_REG {
  union{
    u32 reg32;
    // struct  { u32 rsv0:19, STREAME:1,  ACQS:1, ACQT:1, ACQK:1, ACQE:1, STRG:1, TRGS:1, rsv1:1,
	// DMAE:1, rsv4:1, ERRiE:1, DMAiE:1, ACQiE:1, CHOP_ON:1, CHOP_DEFAULT:1;
      //    struct  { u32 rsv0:23, ACQE:1, rsv1:3, DMAE:1, rsv4:1, ERRiE:1, DMAiE:1, ACQiE:1;
    struct  {u32 rsv0:10, CHOP_ON:1, CHOP_DEFAULT:1, CHOP_RECONSTRUCT:1, OFFSET_CALC:1, INTEGRAL_CALC:1, DAC_SHIFT:4,
    	STREAME:1, ACQS:1, ACQT:1, ACQK:1, ACQE:1, STRG:1, TRGS:1, rsv2:1, DMAE:1, CH32_CHOP:1, ERRiE:1, DMAiE:1, ACQiE:1;
    } cmdFlds;
  };    
} COMMAND_REG;

typedef struct _DMA_REG {
    union
    {
      u32 reg32;
      struct  {
	u32 Size:16, BuffsNumber:16;
      } dmaFlds;
    };
} DMA_REG;

typedef struct _PCIE_HREGS {
  volatile STATUS_REG       status;
  volatile COMMAND_REG      command;            /*Offset 0x04*/
  volatile u32              acqFreq;            /*Offset 0x08*/
  volatile DMA_REG          dmaReg;             /*Offset 0x0C*/
  volatile u32              dmaCurrBuff;        /*Offset 0x10*/
  volatile u32              hwcounter;          /*Offset 0x14*/
  volatile u32              triggerDelay;      /*Offset 0x18*/
  volatile u32              _reserved2;      /*Offset 0x1C*/
  volatile u32              HwDmaAddr[16];      /*Offset 0x20 - 0x5C*/
  volatile u32              dmaNbytes;          /*Offset 0x60*/
  volatile u32              dmaOffSet;          /*Offset 0x64*/
  volatile u32              _reserved3;         /*Offset 0x68*/
  volatile u32              _reserved4;         /*Offset 0x6C*/
  volatile u32              acqByteSize;        /*Offset 0x70 - Default=0x4000000 (64Mb)*/
  /* new registers for chopper operation */
  volatile u32              _reserved5;       	/*Offset 0x74 */
  volatile u32              dac_data[8];        /*Offset 0x78-0x94 (d30)*/
  volatile u32              _reserved6[2];      /*Offset 0x98-0x9C */
  volatile u32              chop_max_count;     /*Offset 0xA0 (d40)*/
  volatile u32              chop_change_count;  /*Offset 0xA4 */

} PCIE_HREGS;

/*Structure for pcie access*/
typedef struct _PCIE_DEV {
  /* char device */
  struct cdev cdev;     /* linux char device structure   */
  struct pci_dev *pdev; /* pci device */
  dev_t devno;          /* char device number */
  struct device *dev;
  unsigned char irq;
  spinlock_t irq_lock; //static
  unsigned int counter;
  unsigned int counter_hw;
  unsigned int open_count;
  struct semaphore open_sem;    //mutual exclusion semaphore
  wait_queue_head_t rd_q;       //read  queues
  long wt_tmout;                //read timeout
  atomic_t rd_condition;
  unsigned int mismatches;
  unsigned int max_buffer_count;
  unsigned int curr_buf;

  BAR_STRUCT memIO[2];
  DMA_STRUCT dmaIO;
  READ_BUF bufRD;               // buffer struct for read() ops
  PCIE_HREGS *pHregs;

} PCIE_DEV ;

/*function prototypes*/
ssize_t pcieAdc_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
ssize_t pcieAdc_write(struct file *file, const char *buf, size_t count, loff_t * ppos);
int pcieAdc_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
int pcieAdc_open(struct inode *inode, struct file *filp);
int pcieAdc_release(struct inode *inode, struct file *filp);
int pcieAdc_probe(struct pci_dev *pdev, const struct pci_device_id *id) ;
void pcieAdc_remove(struct pci_dev *pdev);
	  
#endif /*PCIEADC_H_*/
