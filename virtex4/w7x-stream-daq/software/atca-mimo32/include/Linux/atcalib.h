/*
 * atcalib.h
 * v 0.1 july 2011 mmz
 */

//#define DMA_SIZE 131072 // 128k 
//#define DMA_SIZE 1048576 // 1Mb 
#define DMA_SIZE 4194304 // 4Mb 
#define NUM_CHAN 32	// nr of 16bit data fields per sample. Can be 32 channels or 2 counters and 30 channels, etc.
#define TIMERVALUE 1000000
#define N_AQS DMA_SIZE/(NUM_CHAN*2)  // nr samples per buffer (IRQ)
#define N_PACKETS 2  // size in full buffers (4Mb) of data to save to disk
#define N_PACKETS_2 16  // size in full buffers (4Mb) of data to save to disk

// void sigint_handler(int sig, int *run); 		// SIGINT handler
int init_device(char *devname, int chopped, unsigned int chopperFreq); 	// device initialization
void enable_save_to_disk(int *nrpk, int *nraq, int *acq);
void save_to_disk(int *nrpk, int *nraq, int *acq, short *acqData, short *matData);
void save_to_disk2 (int nrOfReads, short *acqData);
unsigned int concatenate(short a, short b);





